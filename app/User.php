<?php

namespace App;

use Illuminate\Database\Eloquent\Collection;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Zizaco\Entrust\Traits\EntrustUserTrait;
use Carbon\Carbon;


class User extends Authenticatable
{
    use EntrustUserTrait;

    protected $table = 'users';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'email',
        'phone',
        'password',
        'gender',
        'birthday',
        'description',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * Set birthday attribute
     *
     * @param $date
     */
    public function setBirthdayAttribute($date)
    {
        $this->attributes['birthday'] = Carbon::parse($date);
    }

    public function getBirthdayAttribute($date)
    {
        return new Carbon($date);
    }

    /**
     * Get user roles
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function roles() {
        return $this->belongsToMany('App\Role');
    }

    /**
     * Get a list of ids of user roles
     *
     * @return array
     */
    public function getRoleListAttributes() {
        return $this->roles->lists('name');
    }

    /**
     * Check if user has specified role
     *
     * @param $names
     * @return bool
     */
    public function hasRole($names) {
        if (!is_array($names)) {
           $names = array($names);
        }

        foreach ($names as $name) {
            foreach ($this->roles()->get() as $role) {
                if ($role->name == $name) {
                    return true;
                }
            }
        }

        return false;
    }

    /**
     * @param string $ability
     * @param array $arguments
     * @return null
     */
    public function can($ability, $arguments = []) {
        return null;
    }


    /**
     * @param $roles
     * @param $permissions
     * @param $options
     */
    public function ability($roles, $permissions, $options) {

    }

    /**
     * Created many groups
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function groups() {
        return $this->hasMany('App\Group', 'user_id');
    }

    /**
     * Created many tasks
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function tasks() {
        return $this->hasMany('App\Task');
    }

    /**
     * Get all study groups where user is student
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function studyInGroups() {
        return $this->belongsToMany('App\Group', 'group_user', 'user_id', 'group_id');
    }

    /**
     * Check if user is studying in specified group
     *
     * @param $group
     * @return bool
     */
    public  function isStudentInGroup($group) {
        foreach ($this->studyInGroups()->get() as $myGroup) {
            if ($myGroup->id == $group->id)
                return true;
        }

        return false;
    }

    /**
     * Get all tasks in studying groups
     */
    public function studyTasks() {
        $tasks = new Collection;
        foreach ($this->studyInGroups()->get() as $group) {
            $tasks = $tasks->merge($group->tasks);
        }

        return $tasks;
    }

    /**
     * Check if tasks is done
     *
     * @param $task
     * @return bool
     */
    public function isTaskDone($task) {
        $solution = $this->getSolutionForTask($task);

        if ($solution == null)
            return false;

        return $solution->isDone();
    }

    /**
     * Get study materials uploaded by user
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function materials() {
        return $this->hasMany('App\StudyMaterial', 'user_id');
    }

    /**
     * Get user's(student's) submitted solutions
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function solutions() {
        return $this->hasMany('App\Solution', 'user_id');
    }

    /**
     * Get user's(teacher's) submitted feedback
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function solutionFeedbacks() {
        return $this->hasMany('App\SolutionFeedback', 'user_id');
    }

    /**
     * Check if user has uploaded solution for task
     *
     * @param Task $task
     * @return bool
     */
    public function hasSolutionForTask(Task $task) {
        return $this->getSolutionForTask($task) != null;
    }

    /**
     * Get solution for specified task
     *
     * @param Task $task
     * @return mixed
     */
    public function getSolutionForTask(Task $task) {
        $solution = $this->solutions()->get()->where('task_id', $task->id)->first();

        return $solution;
    }
}
