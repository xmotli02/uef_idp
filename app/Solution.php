<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class Solution extends Model
{
    protected $fillable = [
        'subject',
        'description',
        'filename',
        'mime',
        'original_filename',
        'points',
        'published_at'
    ];

    protected $dates = ['published_at'];

    /**
     * Set published_at attribute
     *
     * @param $date
     */
    public function setPublishedAtAttribute($date)
    {
        $this->attributes['published_at'] = Carbon::parse($date);
    }

    public function getPublishedAtAttribute($date)
    {
        return new Carbon($date);
    }

    /**
     * Belongs to study group
     */
    public function task()
    {
        return $this->belongsTo('App\Task');
    }

    /**
     * Was created by user
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function student()
    {
        return $this->belongsTo('App\User', 'user_id');
    }

    /**
     * Check if uploaded solution is done
     */
    public function isDone() {
        $feedback = $this->feedback()->first();
        if ($feedback == null || $feedback->points == 0) {
            return false;
        }

        return true;
    }

    /**
     * Get feedback for submitted solution
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function feedback() {
        return $this->hasOne('App\SolutionFeedback', 'solution_id');
    }

    /**
     * Check if feedback has been published
     *
     * @return mixed
     */
    public function isFeedbackPublished() {
        return $this->feedback()->get()->count() > 0;
    }
}
