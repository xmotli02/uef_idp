<?php

namespace App\Http\Controllers;

use App\Http\Requests\GroupRequest;
use Illuminate\Http\Request;
use App\Http\Controllers\TasksController;

use App\Http\Requests;
use App\Group;
use App\User;
use Illuminate\Support\Facades\Auth;

class GroupsController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('role:admin|teacher', ['only' => ['create', 'store']]);
        $this->middleware('role:admin', ['only' => ['destroy']]);
        $this->middleware('isOwnerOrAdmin', ['only' => ['edit', 'update']]);
    }

    /**
     * Show the all groups in application.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $groups = Group::all();

        return view('pages.groups.index', compact('groups'));
    }

    /**
     * Show group detail
     *
     * @return string
     */
    public function show($id)
    {
        $group = Group::findOrFail($id);
        $tasks = $group->tasks()->get();

        return view('pages.groups.show', compact('group', 'tasks'));
    }

    /**
     * Create new study group
     */
    public function create()
    {
        return view('pages.groups.create');
    }

    public function store(GroupRequest $request) {
        $group = new Group($request->All());

        Auth::user()->groups()->save($group);

        flash('Your study group has been successfully created', 'success');

        return redirect('groups');
    }

    /**
     * Edit group info
     */
    public function edit($id)
    {
        $group = Group::findOrFail($id);

        return view('pages.groups.edit', compact('group'));
    }

    public function update($id, GroupRequest $request) {
        $group = Group::findOrFail($id);

        $group->update($request->all());

        flash('Your study group has been successfully updated', 'success');

        return redirect('groups');
    }

    /**
     * Delete study group, only allowed to admin user
     *
     * @param $id
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id) {
        $group = Group::findOrFail($id);
        $group->delete();

        flash('Study group has been successfully deleted', 'danger');

        return redirect('groups');
    }

    public function addStudentToGroup($groupId, $userId) {
        $group = Group::findOrFail($groupId);
        $student = User::findOrFail($userId);

        $group->students()->save($student);

        flash('You have been added to study group: ' . $group->name, 'success');

        return redirect('groups');
    }

    public function removeStudentFromGroup($groupId, $userId) {
        $group = Group::findOrFail($groupId);
        $student = User::findOrFail($userId);

        $group->students()->detach($student);

        flash('You have been removed from study group: ' . $group->name, 'danger');

        return redirect('groups');
    }
}
