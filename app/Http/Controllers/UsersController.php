<?php

namespace App\Http\Controllers;

use App\User;
use App\Role;
use App\Http\Requests\UserRequest;
use Image;
use Illuminate\Http\Request;
use App\Http\Requests;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;


class UsersController extends Controller
{
    /**
     * Create a new controller instance.
     *
     */
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('role:admin', ['only' => ['destroy']]);
        $this->middleware('isAccountOwnerOrAdmin', ['only' => ['edit']]);
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $users = User::all();

        return view('pages.users.index', compact('users'));
    }

    /**
     * Show user profile
     *
     * @return string
     */
    public function show($id)
    {
        $user = User::findOrFail($id);

        return view('pages.users.show', compact('user'));
    }

    /**
     * Edit user info, roles, permissions
     */
    public function edit($id)
    {
        $user = User::findOrFail($id);
        $roles = Role::pluck('display_name', 'name');
        $userRole = $user->getRoleListAttributes()->first();

        return view('pages.users.edit', compact('user', 'roles', 'userRole'));
    }

    public function update($id, UserRequest $request) {
        $user = User::findOrFail($id);

        $user->update($request->all());

        if ($request->get('roles') != null) {
            $user->detachRoles();
            $userRole = Role::where('name', $request->get('roles'))->first();
            $user->roles()->save($userRole);
        }

        if ($request->hasFile('file')) {
            $avatar = $request->file('file');
            $filename = time() . $id . "." .$avatar->getClientOriginalExtension();
            Image::make($avatar)->resize(300,300)->save( public_path('/uploads/avatars/' . $filename));
            $user->avatar = $filename;
            $user->save();
        }


        flash('User information has been successfully updated', 'success');

        return redirect('users');
    }

    public function destroy($id) {
        $user = User::findOrFail($id);
        $user->delete();

        flash('User has been successfully deleted', 'danger');

        return redirect('users');
    }
}
