<?php

namespace App\Http\Controllers;

use App\Http\Requests\SolutionRequest;
use App\Solution;

use App\Http\Requests;
use App\Task;
use Carbon\Carbon;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;

class SolutionsController extends Controller
{
    /**
     * Create a new controller instance.
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show solution details
     *
     * @param $id
     * @return string
     */
    public function show($id)
    {
        $solution = Solution::findOrFail($id);

        return view('pages.solutions.show', compact('solution'));
    }

    /**
     * Save new solution
     */
    public function create($id)
    {
        $task = Task::findOrFail($id);

        return view('pages.solutions.create', compact('task'));
    }

    /**
     * Store new solution
     *
     * @param $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store($id, SolutionRequest $request)
    {
        $file = $request->file('file');

        if ($file != null) {
            $extension = $file->getClientOriginalExtension();
            Storage::disk('local')->put($file->getFilename() . '.' . $extension, File::get($file));

            $request->merge(array('mime' => $file->getClientMimeType(),
                'original_filename' => $file->getClientOriginalName(),
                'filename' => $file->getFilename() . '.' . $extension));
        }

        $solution = new Solution($request->All());
        $solution->published_at = Carbon::now();

        Auth::user()->solutions()->save($solution);

        $task = Task::findOrFail($id);
        $task->solutions()->save($solution);

        flash('Your solution for task: ' . $task->name .' has been saved', 'success');

        return view('pages.solutions.show', compact('solution'));
    }

    /**
     * Edit solution
     *
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit($id)
    {
        $solution = Solution::findOrFail($id);

        return view('pages.solutions.edit', compact('solution'));
    }

    /**
     * Update solution
     *
     * @param $id
     * @param SolutionRequest $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function update($id, SolutionRequest $request)
    {
        $solution = Solution::findOrFail($id);

        $file = $request->file('file');
        if ($file != null) {
            $this->deleteFileFromStorage($solution->filename);

            $extension = $file->getClientOriginalExtension();
            Storage::disk('local')->put($file->getFilename() . '.' . $extension, File::get($file));

            $request->merge(array('mime' => $file->getClientMimeType(),
                'original_filename' => $file->getClientOriginalName(),
                'filename' => $file->getFilename() . '.' . $extension));
        }

        $solution->update($request->all());
        $solution->published_at = Carbon::now();

        flash('Your solution for task: ' . $solution->task->name .' has been saved', 'success');

        return view('pages.solutions.show', compact('solution'));
    }

    /**
     * Get solution file
     *
     * @param $filename
     * @return $this
     */
    public function get($filename)
    {
        $solution = Solution::where('filename', '=', $filename)->firstOrFail();
        $file = Storage::disk('local')->get($solution->filename);

        return (new Response($file, 200))
            ->header('Content-Type', $solution->mime);
    }
}
