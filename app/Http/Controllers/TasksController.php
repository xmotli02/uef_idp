<?php

namespace App\Http\Controllers;

use App\Http\Requests\TaskRequest;
use App\Task;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Group;
use Illuminate\Support\Facades\Auth;

class TasksController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('isOwnerOrAdmin', ['only' => ['edit', 'update', 'destroy']]);
        $this->middleware('role:admin|teacher', ['only' => ['create', 'store']]);
    }

    /**
     * Show the all tasks in application.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $tasks = Task::orderBy('deadline', 'asc')
            ->published()
            ->deadline()
            ->get();

        return view('pages.tasks.index', compact('tasks'));
    }

    /**
     * Show task detail
     *
     * @param $id
     * @return string
     */
    public function show($id)
    {
        $task = Task::findOrFail($id);

        return view('pages.tasks.show', compact('task'));
    }

    /**
     * Create new task
     */
    public function create($id)
    {
        $group = Group::findOrFail($id);

        return view('pages.tasks.create', compact('group'));
    }

    /**
     * @param TaskRequest $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store($id, TaskRequest $request) {
        $task = new Task($request->All());

        $group = Group::findOrFail($id);
        $group->tasks()->save($task);

        Auth::user()->tasks()->save($task);

        flash('Your task has been created', 'success');

        return view('pages.tasks.show', compact('task'));
    }

    /**
     * Edit task info
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit($id)
    {
        $task = Task::findOrFail($id);

        return view('pages.tasks.edit', compact('task'));
    }

    /**
     * @param $id
     * @param TaskRequest $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update($id, TaskRequest $request) {
        $task = Task::findOrFail($id);

        $task->update($request->all());

        flash('Your task has been updated', 'success');

        return view('pages.tasks.show', compact('task'));
    }

    /**
     * Delete task
     *
     * @param $id
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id) {
        $task = Task::findOrFail($id);
        $group = $task->group;

        $task->delete();

        $tasks = $group->tasks()->get();

        flash('Task has been successfully deleted', 'danger');

        return view('pages.groups.show', compact('group', 'tasks'));
    }
}
