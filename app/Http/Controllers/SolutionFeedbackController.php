<?php

namespace App\Http\Controllers;

use App\Http\Requests\SolutionFeedbackRequest;
use App\Solution;
use App\SolutionFeedback;

use App\Http\Requests;
use Illuminate\Support\Facades\Auth;

class SolutionFeedbackController extends Controller
{
    /**
     * Create a new controller instance.
     */
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('role:admin|teacher');
//        $this->middleware('isOwnerOrAdmin', ['only' => ['edit']]);
    }


    public function create($id)
    {
        $solution = Solution::findOrFail($id);

        return view('pages.feedback.create', compact('solution'));
    }

    /**
     * Store new solution feedback
     *
     * @param $id
     * @param SolutionFeedbackRequest $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function store($id, SolutionFeedbackRequest $request)
    {
        $feedback = new SolutionFeedback($request->All());

        Auth::user()->solutionFeedbacks()->save($feedback);

        $solution = Solution::findOrFail($id);
        $solution->feedback()->save($feedback);

        flash('Your feedback has been saved', 'success');

        return view('pages.solutions.show', compact('solution'));
    }

    /**
     * Edit feedback
     *
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit($id)
    {
        $solution = Solution::findOrFail($id);

        $feedback = $solution->feedback;
        if ($feedback == null) {
            $feedback = new SolutionFeedback();
        }

        return view('pages.feedbacks.edit', compact('solution', 'feedback'));
    }

    /**
     * Update feedback
     *
     * @param $id
     * @param SolutionFeedbackRequest $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function update($id, SolutionFeedbackRequest $request)
    {
        $solution = Solution::findOrFail($id);

        $solution->feedback->update($request->all());

        flash('Your feedback has been updated', 'success');

        return view('pages.solutions.show', compact('solution'));
    }
}
