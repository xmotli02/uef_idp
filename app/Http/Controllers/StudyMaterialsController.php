<?php

namespace App\Http\Controllers;

use App\Group;
use App\Http\Requests\StudyMaterialRequest;
use App\StudyMaterial;
use Illuminate\Http\Request;

use App\Http\Requests;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;

class StudyMaterialsController extends Controller
{
    /**
     * Create a new controller instance.
     *
     */
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('isOwnerOrAdmin', ['only' => ['edit', 'destroy']]);
    }

    /**
     * Get all study materials
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $materials = StudyMaterial::all();

        return view('pages.materials.index', compact('materials'));
    }

    /**
     * Show study material detail
     *
     * @param $id
     * @return string
     */
    public function show($id)
    {
        $material = StudyMaterial::findOrFail($id);

        return view('pages.materials.show', compact('material'));
    }

    /**
     * Save new study material
     */
    public function create($id)
    {
        $group = Group::findOrFail($id);

        return view('pages.materials.create', compact('group'));
    }

    /**
     * Store new study material
     *
     * @param $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store($id, StudyMaterialRequest $request)
    {
        $file = $request->file('file');

        if ($file != null) {
            $extension = $file->getClientOriginalExtension();
            Storage::disk('local')->put($file->getFilename() . '.' . $extension, File::get($file));

            $request->merge(array('mime' => $file->getClientMimeType(),
                'original_filename' => $file->getClientOriginalName(),
                'filename' => $file->getFilename() . '.' . $extension));
        }

        $material = new StudyMaterial($request->All());

        Auth::user()->materials()->save($material);

        $group = Group::findOrFail($id);
        $group->materials()->save($material);

        flash('Your study material has been added to ' . $group->name, 'success');

        return view('pages.materials.show', compact('material'));
    }

    /**
     * Edit study material
     *
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit($id)
    {
        $material = StudyMaterial::findOrFail($id);

        return view('pages.materials.edit', compact('material'));
    }

    /**
     * Update study material
     *
     * @param $id
     * @param StudyMaterialRequest $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function update($id, StudyMaterialRequest $request)
    {
        $material = StudyMaterial::findOrFail($id);

        $file = $request->file('file');
        if ($file != null) {
            $this->deleteFileFromStorage($material->filename);

            $extension = $file->getClientOriginalExtension();
            Storage::disk('local')->put($file->getFilename() . '.' . $extension, File::get($file));

            $request->merge(array('mime' => $file->getClientMimeType(),
                'original_filename' => $file->getClientOriginalName(),
                'filename' => $file->getFilename() . '.' . $extension));
        }

        $material->update($request->all());

        flash('Your study material has been successfully updated', 'success');

        return view('pages.materials.show', compact('material'));
    }

    /**
     * Get study material
     *
     * @param $filename
     * @return $this
     */
    public function get($filename)
    {
        $material = StudyMaterial::where('filename', '=', $filename)->firstOrFail();
        $file = Storage::disk('local')->get($material->filename);

        return (new Response($file, 200))
            ->header('Content-Type', $material->mime);
    }

    /**
     * Delete study material
     *
     * @param $id
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        $material = StudyMaterial::findOrFail($id);
        $this->deleteFileFromStorage($material->filename);
        $material->delete();

        flash('Your study material has been deleted', 'danger');

        return redirect('study-materials');
    }

    /**
     * Delete file saved from server storage
     *
     * @param $filename
     */
    private function deleteFileFromStorage($filename) {
        if ($filename != '') {
            Storage::delete($filename);
        }
    }
}
