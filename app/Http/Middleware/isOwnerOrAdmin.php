<?php

namespace App\Http\Middleware;

use App\StudyMaterial;
use Closure;
use App\Group;
use App\Task;
use Illuminate\Support\Facades\Auth;

class isOwnerOrAdmin
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        // is admin
        if (Auth::user()->hasRole('admin')) {
            return $next($request);
        }

        if ($request->is('groups/*')) {
            $user = Group::findOrFail($request->id)->owner;
        }
        else if ($request->is('tasks/*')) {
            $user = Task::findOrFail($request->id)->owner;
        }
        else if ($request->is('study-materials/*')) {
            $user = StudyMaterial::findOrFail($request->id)->owner;
        }
        else {
            response('Unauthorized (isOwnerOrAdmin).', 401);
        }

        //is owner
        if($user->id == Auth::user()->id)
        {
            return $next($request);
        }

        return response('Unauthorized.', 401);
    }
}
