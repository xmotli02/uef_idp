<?php

namespace App\Http\Middleware;

use Closure;
use App\User;
use Illuminate\Support\Facades\Auth;

class isAccountOwnerOrAdmin
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        // is admin
        if (Auth::user()->hasRole('admin')) {
            return $next($request);
        }

        $user = User::findOrFail($request->id);

        //is owner
        if($user->id == Auth::user()->id)
        {
            return $next($request);
        }

        return response('Unauthorized.', 401);
    }
}
