<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', 'HomeController@index');

Route::auth();

/**
 * Routes for users
 */
Route::get('/users', 'UsersController@index');
Route::get('/users/{id}', 'UsersController@show');
Route::get('/users/{id}/edit', 'UsersController@edit');
Route::patch('/users/{id}', 'UsersController@update');
Route::delete('/users/{id}', 'UsersController@destroy');

/**
 * Routes for study groups
 */
Route::get('/groups', 'GroupsController@index');
Route::get('/groups/create', 'GroupsController@create');
Route::post('/groups', 'GroupsController@store');
Route::get('/groups/{id}', 'GroupsController@show');
Route::get('/groups/{id}/edit', 'GroupsController@edit');
Route::patch('/groups/{id}', 'GroupsController@update');
Route::delete('/groups/{id}', 'GroupsController@destroy');
Route::patch('/groups/{id}/addStudent/{userId}', 'GroupsController@addStudentToGroup');
Route::patch('/groups/{id}/removeStudent/{userId}', 'GroupsController@removeStudentFromGroup');

/**
 * Routes for study materials
 */
Route::get('/study-materials', 'StudyMaterialsController@index');
Route::get('/study-materials/{id}', 'StudyMaterialsController@show');
Route::get('/study-materials/get/{filename}', 'StudyMaterialsController@get');
Route::get('/groups/{id}/study-materials/create', 'StudyMaterialsController@create');
Route::post('/groups/{id}/study-materials', 'StudyMaterialsController@store');
Route::get('/study-materials/{id}/edit', 'StudyMaterialsController@edit');
Route::patch('/study-materials/{id}', 'StudyMaterialsController@update');
Route::delete('/study-materials/{id}', 'StudyMaterialsController@destroy');

/**
 * Routes for tasks
 */
Route::get('/tasks', 'TasksController@index');
Route::get('/tasks/{id}', 'TasksController@show');
Route::get('/groups/{id}/tasks/create', 'TasksController@create');
Route::post('/groups/{id}', 'TasksController@store');
Route::get('/tasks/{id}/edit', 'TasksController@edit');
Route::patch('/tasks/{id}', 'TasksController@update');
Route::delete('/tasks/{id}', 'TasksController@destroy');

/**
 * Routes for tasks solutions
 */
Route::get('/solutions/{id}', 'SolutionsController@show');
Route::get('/solutions/get/{filename}', 'SolutionsController@get');
Route::get('/tasks/{id}/solutions/create', 'SolutionsController@create');
Route::post('/tasks/{id}', 'SolutionsController@store');
Route::get('/solutions/{id}/edit', 'SolutionsController@edit');
Route::patch('/solutions/{id}', 'SolutionsController@update');

/**
 * Routes for solutions feedback
 */
Route::get('/solutions/{id}/feedback/create', 'SolutionFeedbackController@create');
Route::post('/solutions/{id}', 'SolutionFeedbackController@store');
Route::get('/solutions/{id}/feedback/edit', 'SolutionFeedbackController@edit');
Route::patch('/solutions/{id}', 'SolutionFeedbackController@update');


/**
 * Temporary
 */
Route::get('/charts', function()
{
	return View::make('mcharts');
});

Route::get('/tables', function()
{
	return View::make('table');
});

Route::get('/forms', function()
{
	return View::make('form');
});

Route::get('/grid', function()
{
	return View::make('grid');
});

Route::get('/buttons', function()
{
	return View::make('buttons');
});


Route::get('/icons', function()
{
	return View::make('icons');
});

Route::get('/panels', function()
{
	return View::make('panel');
});

Route::get('/typography', function()
{
	return View::make('typography');
});

Route::get('/notifications', function()
{
	return View::make('notifications');
});

Route::get('/blank', function()
{
	return View::make('blank');
});

Route::get('/documentation', function()
{
	return View::make('documentation');
});