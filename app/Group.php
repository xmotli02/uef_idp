<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Group extends Model
{
    protected $table = 'groups';

    protected $fillable = [
        'name',
        'description'
    ];

    /**
     * Was created by user
     */
    public function owner() {
        return $this->belongsTo('App\User', 'user_id');
    }

    /**
     * Has many students
     */
    public function students() {
        return $this->belongsToMany('App\User', 'group_user', 'group_id', 'user_id');
    }

    /**
     * Has many tasks
     */
    public function tasks() {
        return $this->hasMany('App\Task');
    }


    /**
     * Get study materials for group
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function materials() {
        return $this->hasMany('App\StudyMaterial', 'group_id');
    }
}
