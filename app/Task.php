<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class Task extends Model
{
    protected $fillable = [
        'name',
        'description',
        'points',
        'published_at',
        'deadline'
    ];

    protected $dates = ['published_at', 'deadline'];

    /**
     * Scope queries to tasks that have been published
     *
     * @param $query
     */
    public function scopePublished($query)
    {
        $query->where('published_at', '<=', Carbon::now());
    }

    /**
     * Scope queries to tasks that are actual
     *
     * @param $query
     */
    public function scopeDeadline($query)
    {
        $query->where('deadline', '>', Carbon::now());
    }

    /**
     * Set published_at attribute
     *
     * @param $date
     */
    public function setPublishedAtAttribute($date)
    {
        $this->attributes['published_at'] = Carbon::parse($date);
    }

    public function getPublishedAtAttribute($date)
    {
        return new Carbon($date);
    }

    /**
     * Set deadline attribute
     *
     * @param $date
     */
    public function setDeadlineAttribute($date)
    {
        $this->attributes['deadline'] = Carbon::parse($date);
    }

    /**
     * Belongs to study group
     */
    public function group()
    {
        return $this->belongsTo('App\Group');
    }

    /**
     * Was created by user
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function owner()
    {
        return $this->belongsTo('App\User', 'user_id');
    }

    /**
     * Get all submitted solution for task
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function solutions() {
        return $this->hasMany('App\Solution', 'task_id');
    }

    /**
     * Check deadline of task
     *
     * @return mixed
     */
    public function isOverDeadline() {
        return $this->deadline->lt(Carbon::now());
    }
}
