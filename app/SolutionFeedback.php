<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SolutionFeedback extends Model
{
    protected $table = 'solution_feedbacks';

    protected $fillable = [
        'comment',
        'points'
    ];

    public function solution() {
        return $this->belongsTo('App\Solution', 'solution_id');
    }

    public function teacher() {
        return $this->belongsTo('App\User', 'user_id');
    }
}