@extends('layouts.dashboard')
@section('page_heading','All study materials')
@section('section')
    @include('pages.materials._materialsTable', array('class'=>'table-hover'))
@endsection