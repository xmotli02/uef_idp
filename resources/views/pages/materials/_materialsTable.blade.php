<table class="table {{ $class }}">
    <thead>
    <tr>
        <th></th>
        <th>Name</th>
        <th>Description</th>
        <th>Teacher</th>
        @if(Route::getCurrentRoute()->getPath() === 'study-materials')
            <th>Study group</th>
        @endif
        @if (Auth::User()->hasRole(['admin', 'teacher']))
            <th>Edit</th>
            <th>Delete</th>
        @endif
    </tr>
    </thead>
    <tbody>
    @foreach($materials as $material)
        <tr>
            <td>
                {{-- If there is any file uploaded --}}
                @if ($material->filename != '')
                    <a href="{{ action('StudyMaterialsController@get', [$material->filename]) }}" data-placement="top"
                        data-toggle="tooltip" title="Download">
                        <i class="fa fa-download"></i>
                    </a>
                @endif
            </td>
            <td>
                <a href="{{ action('StudyMaterialsController@show', [$material->id]) }}">{{ $material->name }}</a>
            </td>
            <td>{{ substr($material->description, 0, 40) }} ...</td>
            <td>
                <a href="{{ action('UsersController@show', [$material->owner->id]) }}">
                    {{ $material->owner->name }}
                </a>
            </td>
            @if(Route::getCurrentRoute()->getPath() === 'study-materials')
                <td>
                    <a href="{{ action('GroupsController@show', [$material->group->id]) }}">
                        {{ $material->group->name }}
                    </a>
                </td>
            @endif
            @if (Auth::User()->hasRole('admin') || $material->owner->id == Auth::User()->id)
                <td>
                    <a href="{{ action('StudyMaterialsController@edit', [$material->id]) }}"
                       data-placement="top" data-toggle="tooltip" title="Edit">
                        <button class="btn btn-primary btn-xs" data-title="Edit" data-toggle="modal">
                            <span class="glyphicon glyphicon-pencil"></span>
                        </button>
                    </a>
                </td>
                <td>
                    {!! Form::open(['method'  => 'delete',
                                    'action' => [ 'StudyMaterialsController@destroy', $material->id ] ]) !!}
                    {!! Form::button('<span class="glyphicon glyphicon-trash"></span>',
                        array('type' => 'submit', 'class' => 'btn btn-danger btn-xs')) !!}
                    {!! Form::close() !!}
                </td>
            @else
                <td></td>
                <td></td>
            @endif
        </tr>
    @endforeach
    </tbody>
</table>