@extends('layouts.dashboard')
@section('page_heading',"Upload study material for group: $group->name")
@section('section')
    <div class="col-lg-8">

        {!! Form::open(['url' => 'groups/'. $group->id . '/study-materials', 'files' => true]) !!}
            @include('pages.materials._form', ['submitButtonText' => 'Upload'])
        {!! Form::close() !!}
        @include('errors.list')
    </div>
@endsection