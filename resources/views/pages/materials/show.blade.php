@extends('layouts.dashboard')
@section('page_heading',"Study material $material->name")
@section('section')

    <div class="col-lg-8">
        @section ('description_panel_title', 'Description')
        @section ('description_panel_body')
            <p>{{ $material->description }}</p>
        @endsection
        @include('widgets.panel', array('header'=>true, 'as'=>'description'))
    </div>

    <div class="col-lg-4">
        @section ('info_panel_title', 'Info')
        @section ('info_panel_body')
            <dl class="dl-horizontal">
                <dt>Teacher:</dt>
                <dd>
                    <a href="{{ action('UsersController@show', [$material->owner->id]) }}">
                        {{ $material->owner->name }}
                    </a>
                </dd>
                <dt>Study group:</dt>
                <dd>
                    <a href="{{ action('GroupsController@show', [$material->group->id]) }}">
                        {{ $material->group->name }}
                    </a>
                </dd>
            </dl>

        <div class="col-sm-12 ">
            {{-- Download button --}}
            @if ($material->filename != '')
                <div class="col-sm-3 ">
                    <a href="{{ action('StudyMaterialsController@get', [$material->filename]) }}" data-placement="top"
                       data-toggle="tooltip" title="Edit">
                        <button type="button" class="btn btn-default btn-circle btn-lg">
                            <i class="fa fa-download"></i>
                        </button>
                    </a>
                </div>
            @endif

            @if (Auth::User()->hasRole('admin') || $material->owner->id == Auth::User()->id)
                <div class="col-sm-7 pull-right">
                    <div class="col-sm-6 ">
                        <a href="{{ action('StudyMaterialsController@edit', $material->id) }}" data-placement="top"
                           data-toggle="tooltip" title="Edit">
                            <button type="button" class="btn btn-primary btn-circle btn-lg">
                                <i class="fa fa-pencil"></i>
                            </button>
                        </a>
                    </div>
                    <div class="col-sm-6 ">
                        {!! Form::open([    'method'  => 'delete',
                                            'action' => [ 'StudyMaterialsController@destroy', $material->id ] ]) !!}
                        {!! Form::button('<i class="fa fa-trash-o"></i>',
                            array('type' => 'submit', 'class' => 'btn btn-danger btn-circle btn-lg')) !!}
                        {!! Form::close() !!}
                    </div>
                </div>
            @endif
        </div>
        @endsection
        @include('widgets.panel', array('header'=>true, 'as'=>'info'))
    </div>
@endsection