@extends('layouts.dashboard')
@section('page_heading',"Update study material: $material->name")
@section('section')
    <div class="col-lg-8">

        {!! Form::model($material, ['method' => 'PATCH', 'files' => true,
                'action' => ['StudyMaterialsController@update', $material->id]]) !!}
        @include('pages.materials._form', ['submitButtonText' => 'Update'])
        {!! Form::close() !!}

        @include('errors.list')
    </div>
@endsection