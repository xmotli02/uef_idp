<div class="form-group">
    {!! Form::label('points', 'Points:') !!}
    {!! Form::select('points', range(0, $solution->task->max_points), null, ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::label('comment', 'Comment:') !!}
    {!! Form::textarea('comment', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::submit($submitButtonText, ['class' => 'btn btn-primary form-control']) !!}
</div>