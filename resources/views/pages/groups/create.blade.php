@extends('layouts.dashboard')
@section('page_heading',"Create new study group")
@section('section')
    <div class="col-lg-8">

        {!! Form::open(['url' => 'groups']) !!}
            @include('pages.groups._form', ['submitButtonText' => 'Add study group'])
        {!! Form::close() !!}
        @include('errors.list')
    </div>
@endsection