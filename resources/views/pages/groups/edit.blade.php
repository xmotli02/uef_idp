@extends('layouts.dashboard')
@section('page_heading',"Update study group: $group->name")
@section('section')
    <div class="col-lg-8">

        {!! Form::model($group, ['method' => 'PATCH', 'action' => ['GroupsController@update', $group->id]]) !!}
            @include('pages.groups._form', ['submitButtonText' => 'Update study group'])
        {!! Form::close() !!}

        @include('errors.list')
    </div>
@endsection