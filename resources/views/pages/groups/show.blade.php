@extends('layouts.dashboard')
@section('page_heading',"Study group $group->name")
@section('section')

    <div class="col-lg-9">
        @section ('description_panel_title', 'Description')
        @section ('description_panel_body')
            <p>{{ $group->description }}</p>
        @endsection
        @include('widgets.panel', array('header'=>true, 'as'=>'description'))
    </div>

    {{-- Show list of enrolled students --}}
    <div class="col-lg-3">
        @section ('students_panel_title', 'Students')
        @section ('students_panel_body')
            <ul>
                @foreach ($group->students as $student)
                    <li>
                        <a href="{{ action('UsersController@show', [$student->id]) }}" >
                            {{ $student->name }}</a>
                    </li>
                @endforeach
            </ul>
        @endsection
        @include('widgets.panel', array('header'=>true, 'as'=>'students'))
    </div>

    {{-- Show study group tasks --}}
    <div class="col-lg-9">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title">Tasks
                    {{-- Plus icon for adding new tasks --}}
                    @if (Auth::User()->hasRole(['admin', 'teacher']))
                        <div class="panel-control pull-right">
                            <a href="{{ action('TasksController@create', $group->id) }}" class="panelButton">
                                <i class="fa fa-plus"></i>
                            </a>
                        </div>
                    @endif
                </h3>
            </div>

            <div class="panel-body">
                @include('pages.tasks._tasksTable', array('class'=>'table-hover', 'taskList' => $tasks))
            </div>
        </div>
    </div>


    {{-- Show study group materials --}}
    <div class="col-lg-9">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title">Study Materials
                    {{-- For adding new material --}}
                    @if (Auth::User()->hasRole(['admin', 'teacher']))
                        <div class="panel-control pull-right">
                            <a href="{{ action('StudyMaterialsController@create', $group->id) }}" class="panelButton">
                                <i class="fa fa-plus"></i>
                            </a>
                        </div>
                    @endif
                </h3>
            </div>

            <div class="panel-body">
                @include('pages.materials._materialsTable', array('class'=>'table-hover', 'materials' => $group->materials))
            </div>
        </div>
    </div>

@endsection