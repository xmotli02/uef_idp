<table class="table {{ $class }}">
    <thead>
    <tr>
        <th>Name</th>
        <th>Description</th>
        <th>Owner</th>
        <th>Action</th>
        @if (Auth::User()->hasRole('admin'))
            <th>Delete</th>
        @endif
    </tr>
    </thead>
    <tbody>
    @foreach($groupList as $group)
        <tr>
            <td>
                <a href="{{ action('GroupsController@show', [$group->id]) }}">{{ $group->name }}</a>
            </td>
            <td>{{ substr($group->description, 0, 40) }} ...</td>
            <td>
                <a href="{{ action('UsersController@show', [$group->owner->id]) }}">
                    {{ $group->owner->name }}
                </a>
            </td>
            <td>
                @if (Auth::User()->hasRole('admin') || $group->owner->id == Auth::User()->id)
                    <a href="{{ action('GroupsController@edit', [$group->id]) }}" data-placement="top"
                       data-toggle="tooltip" title="Edit">
                        <button class="btn btn-primary btn-xs" data-title="Edit" data-toggle="modal">
                            <span class="glyphicon glyphicon-pencil"></span>
                        </button>
                    </a>
                @endif

                @if (Auth::User()->hasRole('student'))
                    @if (Auth::User()->isStudentInGroup($group))
                        {!! Form::open(['method'  => 'patch',
                                        'action' => [ 'GroupsController@removeStudentFromGroup',
                                                    'groupId' => $group->id,
                                                    'userId' => Auth::User()->id ] ]) !!}
                        {!! Form::button('<span class="glyphicon glyphicon-minus"></span>',
                            array('type' => 'submit', 'class' => 'btn btn-danger btn-xs')) !!}
                        {!! Form::close() !!}
                    @else
                        {!! Form::open(['method'  => 'patch',
                                        'action' => [ 'GroupsController@addStudentToGroup',
                                                    'groupId' => $group->id,
                                                    'userId' => Auth::User()->id ] ]) !!}
                        {!! Form::button('<span class="glyphicon glyphicon-plus"></span>',
                            array('type' => 'submit', 'class' => 'btn btn-success btn-xs')) !!}
                        {!! Form::close() !!}
                    @endif
                @endif
            </td>
            @if (Auth::User()->hasRole('admin'))
                <td>
                    {!! Form::open([    'method'  => 'delete',
                                        'action' => [ 'GroupsController@destroy', $group->id ] ]) !!}
                    {!! Form::button('<span class="glyphicon glyphicon-trash"></span>',
                        array('type' => 'submit', 'class' => 'btn btn-danger btn-xs')) !!}
                    {!! Form::close() !!}
                </td>
            @endif
        </tr>
    @endforeach
    </tbody>
</table>