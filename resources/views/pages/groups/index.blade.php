@extends('layouts.dashboard')
@section('page_heading','Study groups')
@section('section')

    {{-- Study groups which user own or is studying --}}
    @if (Auth::User()->hasRole(['student', 'teacher']))
        @section ('myGroups_panel_title', 'My study groups:')
        @section ('myGroups_panel_body')
            @if (Auth::User()->hasRole('teacher'))
                @include('pages.groups._groupsTable',
                    array('class'=>'table-hover', 'groupList' => Auth::User()->groups))

                <div class="col-sm-1 pull-right">
                    <a href="{{ action('GroupsController@create') }}" data-placement="top"
                       data-toggle="tooltip" title="Create">
                        <button type="button" class="btn btn-success btn-circle btn-lg"><i class="fa fa-plus"></i></button>
                    </a>
                </div>
            @else
                @include('pages.groups._groupsTable',
                    array('class'=>'table-hover', 'groupList' => Auth::User()->studyInGroups))
            @endif
        @endsection
        @include('widgets.panel', array('header'=>true, 'as'=>'myGroups'))
    @endif

    {{-- All study groups --}}
    @section ('allGroups_panel_title', 'All study groups:')
    @section ('allGroups_panel_body')
        <div class="col-sm-12">
            @include('pages.groups._groupsTable',
                array('class'=>'table-hover', 'groupList' => $groups))
        </div>
    @endsection
    @include('widgets.panel', array('header'=>true, 'as'=>'allGroups'))

    @if (Auth::User()->hasRole('admin'))
        <div class="col-sm-1 pull-right">
            <a href="{{ action('GroupsController@create') }}" data-placement="top"
               data-toggle="tooltip" title="Create">
                <button type="button" class="btn btn-success btn-circle btn-lg"><i class="fa fa-plus"></i></button>
            </a>
        </div>
    @endif
@endsection