@section ('cchart11_panel_title','Study groups')
@section ('cchart11_panel_body')

    @foreach($groupList as $group)

    <div>
        <p>
            <strong><a href="{{ action('GroupsController@show', [$group->id]) }}">{{ $group->name }}</a></strong>
            <span class="pull-right text-muted">40% Complete</span>
        </p>

        <div>
            @include('widgets.progress', array('animated'=> true, 'class'=>'default', 'value'=>'40'))
            <span class="sr-only">40% Complete (success)</span>
        </div>

    </div>

    @endforeach
    <a class="text-center" href="{{ action('GroupsController@index') }} ">
        <strong>See All Groups</strong>
        <i class="fa fa-angle-right"></i>
    </a>
@endsection
@include('widgets.panel', array('header'=>true, 'as'=>'cchart11'))