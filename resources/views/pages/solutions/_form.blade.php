<div class="form-group">
    {!! Form::label('subject', 'Subject:') !!}
    {!! Form::text('subject', null, ['class' => 'form-control', $disabled]) !!}
</div>

<div class="form-group">
    {!! Form::label('description', 'Description:') !!}
    {!! Form::textarea('description', null, ['class' => 'form-control', $disabled]) !!}
</div>

<div class="form-group">
    {!! Form::label('file', 'Choose file') !!}
    {!! Form::file('file', null, ['class' => 'form-control', $disabled]) !!}
</div>

<div class="form-group">
    {!! Form::submit($submitButtonText, ['class' => 'btn btn-primary form-control', $disabled]) !!}
</div>