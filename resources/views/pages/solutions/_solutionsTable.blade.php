<table class="table {{ $class }}">
    <thead>
    <tr>
        <th></th>
        <th>Subject</th>
        <th>Solution</th>
        <th>Student</th>
        <th>Uploaded</th>
        <th>Graded</th>
        <th>Edit</th>
    </tr>
    </thead>
    <tbody>
    @foreach($solutions as $solution)
        <tr>
            <td>
                {{-- If there is any file uploaded --}}
                @if ($solution->filename != '')
                    <a href="{{ action('SolutionsController@get', [$solution->filename]) }}" data-placement="top"
                       data-toggle="tooltip" title="Download">
                        <i class="fa fa-download"></i>
                    </a>
                @endif
            </td>
            <td><a href="{{ action('SolutionsController@show', [$solution->id]) }}">{{ $solution->subject }}</a></td>
            <td>{{ substr($solution->description, 0, 20) }} ...</td>
            <td>
                <a href="{{ action('UsersController@show', [$solution->student->id]) }}">
                    {{ $solution->student->name }}</a>
            </td>
            <td>{{ $solution->published_at->format('d.m.Y H:i:s') }}</td>
            <td>
                @if ($solution->isFeedbackPublished())
                    <i class="fa fa-check"></i>
                @else
                    <i class="fa fa-times"></i>
                @endif

            </td>
            <td>
                <a href="{{ action('SolutionsController@show', [$solution->id]) }}"
                   data-placement="top" data-toggle="tooltip" title="Edit">
                    <button class="btn btn-primary btn-xs" data-title="Edit" data-toggle="modal">
                        <span class="glyphicon glyphicon-pencil"></span>
                    </button>
                </a>
            </td>
        </tr>
    @endforeach
    </tbody>
</table>