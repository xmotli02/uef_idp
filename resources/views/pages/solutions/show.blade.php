@extends('layouts.dashboard')
@section('page_heading',"Solution $solution->subject")
@section('section')

    <div class="col-lg-8">
        @section ('description_panel_title', 'Solution')
        @section ('description_panel_body')
            <p>{{ $solution->description }}</p>
        @endsection
        @include('widgets.panel', array('header'=>true, 'as'=>'description'))
    </div>

    <div class="col-lg-4">
        @section ('info_panel_title', 'Info')
        @section ('info_panel_body')
            <dl class="dl-horizontal">
                <dt>Student:</dt>
                <dd>
                    <a href="{{ action('UsersController@show', [$solution->student->id]) }}">
                        {{ $solution->student->name }}
                    </a>
                </dd>
                <dt>Study task:</dt>
                <dd>
                    <a href="{{ action('TasksController@show', [$solution->task->id]) }}">
                        {{ $solution->task->name }}
                    </a>
                </dd>
                <dt>Points:</dt>
                <dd>
                    @if ($solution->isFeedbackPublished())
                        {{ $solution->feedback->points }}
                    @else
                        Not graded!
                    @endif
                </dd>
            </dl>

            <div class="col-sm-12 ">
                {{-- Download button --}}
                @if ($solution->filename != '')
                    <div class="col-sm-3 ">
                        <a href="{{ action('SolutionsController@get', [$solution->filename]) }}"
                           data-placement="top" data-toggle="tooltip" title="Download">
                            <button type="button" class="btn btn-default btn-circle btn-lg">
                                <i class="fa fa-download"></i>
                            </button>
                        </a>
                    </div>
                @endif
            </div>
        @endsection
        @include('widgets.panel', array('header'=>true, 'as'=>'info'))
    </div>

    @if(Auth::User()->hasRole('teacher'))
        <div class="col-lg-12">
            @section ('feedback_panel_title', 'Give feedback')
            @section ('feedback_panel_body')

                @if ($solution->isFeedbackPublished())
                    {!! Form::model($feedback = $solution->feedback, ['method' => 'PATCH',
                        'action' => ['SolutionFeedbackController@update', $feedback->id]]) !!}
                @else
                    {!! Form::model($feedback = new App\SolutionFeedback,
                        [ action('SolutionFeedbackController@create', $feedback->id)]) !!}
                @endif

                @include('pages.feedbacks._form', ['submitButtonText' => 'Submit feedback'])
                {!! Form::close() !!}

                @include('errors.list')
            @endsection

            @if ($solution->isFeedbackPublished())
                @include('widgets.panel', array('class'=>'success', 'header'=>true, 'as'=>'feedback'))
            @else
                @include('widgets.panel', array('class'=>'warning', 'header'=>true, 'as'=>'feedback'))
            @endif
        </div>
    @endif

@endsection