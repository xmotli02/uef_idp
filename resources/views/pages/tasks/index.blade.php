@extends('layouts.dashboard')
@section('page_heading','Study tasks')
@section('section')

    {{-- Student's tasks --}}
    @if (Auth::User()->hasRole('student') && !Auth::User()->studyTasks()->isEmpty())
        @section ('myTasks_panel_title', 'My tasks:')
        @section ('myTasks_panel_body')
            @include('pages.tasks._tasksTable',
                array('class'=>'table-hover', 'taskList' => Auth::User()->studyTasks()))
        @endsection
        @include('widgets.panel', array('header'=>true, 'as'=>'myTasks'))

    {{-- Teacher's created tasks --}}
    @elseif (Auth::User()->hasRole('teacher') && Auth::User()->tasks)
        @section ('myTasks_panel_title', 'My created tasks:')
        @section ('myTasks_panel_body')
            <div class="col-sm-12">
                @include('pages.tasks._tasksTable',
                    array('class'=>'table-hover', 'taskList' => Auth::User()->tasks))
            </div>
        @endsection
        @include('widgets.panel', array('header'=>true, 'as'=>'myTasks'))
    @endif

    <div class="panel-group" id="accordion">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h4 class="panel-title">
                    <a data-toggle="collapse" data-parent="#accordion" href="#collapse">
                        All tasks:
                    </a>
                </h4>
            </div>
            <div id="collapse" class="panel-collapse collapse in">
                <div class="panel-body">
                    @include('pages.tasks._tasksTable',
                        array('class'=>'table-hover', 'taskList' => $tasks))
                </div>
            </div>
        </div>
    </div>



@endsection