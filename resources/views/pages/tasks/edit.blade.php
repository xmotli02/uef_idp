@extends('layouts.dashboard')
@section('page_heading',"Update study task: $task->name")
@section('section')
    <div class="col-lg-8">

        {!! Form::model($task, ['method' => 'PATCH', 'action' => ['TasksController@update', $task->id]]) !!}
            @include('pages.tasks._form', ['submitButtonText' => 'Update task'])
        {!! Form::close() !!}

        @include('errors.list')
    </div>
@endsection