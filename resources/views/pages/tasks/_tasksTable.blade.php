<table class="table {{ $class }}">
    <thead>
    <tr>
        <th>Name</th>
        <th>Description</th>
        <th>Starts at</th>
        <th>Deadline</th>
        <th>Teacher</th>
        @if(Route::getCurrentRoute()->getPath() === 'tasks')
            <th>Study group</th>
        @endif
        @if (Auth::User()->hasRole(['admin', 'teacher']))
            <th>Edit</th>
            <th>Delete</th>
        @else
            <th>Done</th>
        @endif
    </tr>
    </thead>
    <tbody>
    @foreach($taskList as $task)
        <tr>
            <td>
                <a href="{{ action('TasksController@show', [$task->id]) }}">{{ $task->name }}</a>
            </td>
            <td>{{ substr($task->description, 0, 15) }} ...</td>
            <td>{{ $task->published_at->format('d.m.Y') }}</td>
            <td>{{ $task->deadline->format('d.m.Y') }}</td>
            <td>
                <a href="{{ action('UsersController@show', [$task->owner->id]) }}">
                    {{ $task->owner->name }}
                </a>
            </td>
            @if(Route::getCurrentRoute()->getPath() === 'tasks')
                <td>
                    <a href="{{ action('GroupsController@show', [$task->group->id]) }}">
                        {{ $task->group->name }}
                    </a>
                </td>
            @endif
            @if (Auth::User()->hasRole('admin') || $task->owner->id == Auth::User()->id)
                <td>
                    <a href="{{ action('TasksController@edit', [$task->id]) }}" data-placement="top"
                       data-toggle="tooltip" title="Edit">
                        <button class="btn btn-primary btn-xs" data-title="Edit" data-toggle="modal">
                            <span class="glyphicon glyphicon-pencil"></span>
                        </button>
                    </a>
                </td>
                <td>
                    {!! Form::open([    'method'  => 'delete',
                                        'action' => [ 'TasksController@destroy', $task->id ] ]) !!}
                    {!! Form::button('<span class="glyphicon glyphicon-trash"></span>',
                        array('type' => 'submit', 'class' => 'btn btn-danger btn-xs')) !!}
                    {!! Form::close() !!}
                </td>
            @elseif (Auth::User()->hasRole('student'))
                <td>
                    @if (Auth::User()->isTaskDone($task))
                        <button type="button"
                                class="btn btn-success btn-circle">
                            <i class="fa fa-check"></i>
                        </button>
                    @else
                        <button type="button"
                                class="btn btn-danger btn-circle">
                            <i class="fa fa-close"></i>
                        </button>
                    @endif
                </td>
            @else
                <td></td>
                <td></td>
            @endif
        </tr>
    @endforeach
    </tbody>
</table>