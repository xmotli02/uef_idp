@extends('layouts.dashboard')
@section('page_heading',"Create new study task in $group->name")
@section('section')
    <div class="col-lg-8">

        {!! Form::model($task = new App\Task, ['url' => 'groups/'. $group->id]) !!}
            @include('pages.tasks._form', ['submitButtonText' => 'Add new task'])
        {!! Form::close() !!}
        @include('errors.list')
    </div>
@endsection