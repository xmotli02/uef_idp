@extends('layouts.dashboard')
@section('page_heading',"Study task $task->name")
@section('section')

    <div class="col-lg-8">
        @section ('description_panel_title', 'Description')
        @section ('description_panel_body')
            <p>{{ $task->description }}</p>
        @endsection
        @include('widgets.panel', array('header'=>true, 'as'=>'description'))
    </div>

    <div class="col-lg-4">
        @section ('info_panel_title', 'Info')
        @section ('info_panel_body')
            <dl class="dl-horizontal">
                <dt>Teacher:</dt>
                <dd>
                    <a href="{{ action('UsersController@show', [$task->owner->id]) }}">
                        {{ $task->owner->name }}
                    </a>
                </dd>
                <dt>Study group:</dt>
                <dd>
                    <a href="{{ action('GroupsController@show', [$task->group->id]) }}">
                        {{ $task->group->name }}
                    </a>
                </dd>
                <dt>Starts at:</dt>
                <dd>{{ $task->published_at->format('d.m.Y') }}</dd>
                <dt>Deadline:</dt>
                <dd>{{ $task->deadline->format('d.m.Y H:i:s') }}</dd>
                <dt>Max points:</dt>
                <dd>{{ $task->max_points }}</dd>
            </dl>
        @endsection
        @include('widgets.panel', array('header'=>true, 'as'=>'info'))
    </div>

    @if (Auth::User()->hasRole('admin') || $task->owner->id == Auth::User()->id)
        <div class="col-sm-2 pull-right">
            <div class="col-sm-6 ">
                <a href="{{ action('TasksController@edit', $task->id) }}" data-placement="top"
                   data-toggle="tooltip" title="Edit">
                    <button type="button" class="btn btn-primary btn-circle btn-lg">
                        <i class="fa fa-pencil"></i>
                    </button>
                </a>
            </div>
            <div class="col-sm-6 pull-right">
                {!! Form::open([    'method'  => 'delete',
                                    'action' => [ 'TasksController@destroy', $task->id ] ]) !!}
                {!! Form::button('<i class="fa fa-trash-o"></i>',
                    array('type' => 'submit', 'class' => 'btn btn-danger btn-circle btn-lg')) !!}
                {!! Form::close() !!}
            </div>
        </div>
    @endif

    @if (Auth::User()->hasRole('admin') || $task->owner->id == Auth::User()->id)
        <div class="col-lg-10">
            @section ('uploadedSolutions_panel_title', 'Solutions from students')
            @section ('uploadedSolutions_panel_body')
                @include('pages.solutions._solutionsTable',
                    array('class'=>'table-hover', 'solutions' => $task->solutions))
            @endsection
            @include('widgets.panel', array('header'=>true, 'as'=>'uploadedSolutions'))
        </div>
    @elseif(Auth::User()->hasRole('student'))
        {{-- Form for solution --}}
        <div class="col-lg-7">
            @if (Auth::User()->hasSolutionForTask($task))
                @section ('solution_panel_title', 'Update your submited solution')
            @else
                @section ('solution_panel_title', 'Upload your solution')
            @endif

            @section ('solution_panel_body')
                @if (Auth::User()->hasSolutionForTask($task))
                    {!! Form::model($solution = Auth::User()->getSolutionForTask($task),
                        [ action('SolutionsController@edit', $task->id), 'files' => true]) !!}
                @else
                    {!! Form::model($solution = new App\Solution,
                        [ action('SolutionsController@create', $task->id), 'files' => true]) !!}
                @endif

                @if ($task->isOverDeadline())
                    @include('pages.solutions._form',
                        ['submitButtonText' => 'Upload solution', 'disabled' => 'disabled'])
                @else
                    @include('pages.solutions._form',
                        ['submitButtonText' => 'Upload solution', 'disabled' => ''])
                @endif
                {!! Form::close() !!}

                @include('errors.list')
            @endsection
            @include('widgets.panel', array('header'=>true, 'as'=>'solution'))
        </div>

        {{-- Solution feedback --}}
        <div class="col-lg-5">
            @section ('solutionFeedback_panel_title', 'Feedback for your solution')
            @section ('solutionFeedback_panel_body')
                @if ($solution->isFeedbackPublished())
                    <dt>Points:</dt>
                    <dd>{{ $solution->feedback->points }}</dd>
                    <dt>Comment:</dt>
                    <dd>{{ $solution->feedback->comment }}</dd>
                    <dt>Published at:</dt>
                    <dd>{{ $solution->feedback->updated_at->format('d.m.Y') }}</dd>
                @else
                    <dt>Not graded yet!</dt>
                @endif
            @endsection
            @if ($solution->isDone())
                @include('widgets.panel', array('class'=>'success', 'header'=>true, 'as'=>'solutionFeedback'))
            @else
                @include('widgets.panel', array('class'=>'danger', 'header'=>true, 'as'=>'solutionFeedback'))
            @endif
        </div>
    @endif

@endsection