<div class="form-group col-lg-12">
    {!! Form::label('name', 'Name:') !!}
    {!! Form::text('name', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group col-lg-12">
    {!! Form::label('description', 'Description:') !!}
    {!! Form::textarea('description', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group col-lg-12">
    {!! Form::label('max_points', 'Max points for task:') !!}
    {!! Form::text('max_points', null, ['class' => 'form-control']) !!}
</div>
<div class="form-group col-lg-6">
    {!! Form::label('published_at', 'Publish on:') !!}
    {!! Form::input('date', 'published_at', $task->published_at->format('d.m.Y'), ['class' => 'form-control']) !!}
</div>

<div class="form-group col-lg-6">
    {!! Form::label('deadline', 'Set deadline:') !!}
    {!! Form::input('date', 'deadline', date('Y-m-d'), ['class' => 'form-control']) !!}
</div>

<div class="form-group col-lg-12">
    {!! Form::submit($submitButtonText, ['class' => 'btn btn-primary form-control']) !!}
</div>