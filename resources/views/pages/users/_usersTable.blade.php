<table class="table {{ $class }}">
    <thead>
    <tr>
        <th>Photo</th>
        <th>Name</th>
        <th>Email</th>
        <th>Role</th>
        @if (Auth::User()->hasRole('admin'))
            <th>Edit</th>
            <th>Delete</th>
        @endif
    </tr>
    </thead>
    <tbody>
    @foreach($users as $user)
        <tr>
            <td>
                <a href="{{ action('UsersController@show', [$user->id]) }}" class="pull-left">
                    <img src="/uploads/avatars/{{ $user->avatar }}" style="width: 50px; height: 50px; border-radius: 50%; margin-right: 25px"/>
                </a>
            </td>
            <td>
                <a href="{{ action('UsersController@show', [$user->id]) }}">{{ $user->name }}</a>
            </td>
            <td>{{ $user->email }}</td>
            <td>
                @if (count($user->roles))
                    {{ $user->roles->first()->display_name }}
                @endif
            </td>
            @if (Auth::User()->hasRole('admin'))
                <td>
                    <a href="{{ action('UsersController@edit', [$user->id]) }}" data-placement="top"
                       data-toggle="tooltip" title="Edit">
                        <button class="btn btn-primary btn-xs" data-title="Edit" data-toggle="modal">
                            <span class="glyphicon glyphicon-pencil"></span>
                        </button>
                    </a>
                </td>
                <td>
                    {!! Form::open([    'method'  => 'delete',
                                        'action' => [ 'UsersController@destroy', $user->id ] ]) !!}
                    {!! Form::button('<span class="glyphicon glyphicon-trash"></span>',
                        array('type' => 'submit', 'class' => 'btn btn-danger btn-xs')) !!}
                    {!! Form::close() !!}
                </td>
            @endif
        </tr>
    @endforeach
    </tbody>
</table>