@extends('layouts.dashboard')
@section('page_heading','Users')
@section('section')

    <div class="col-sm-12">
        @include('pages.users._usersTable', array('class'=>'table-hover'))
    </div>

@endsection