@extends('layouts.dashboard')
@section('page_heading',"Edit user: $user->name")
@section('section')
    <div class="col-lg-6">

    {!! Form::model($user, ['method' => 'PATCH', 'files' => true, 'action' => ['UsersController@update', $user->id]]) !!}
        <div class="form-group">
            {!! Form::label('name', 'Name:') !!}
            {!! Form::text('name', null, ['class' => 'form-control']) !!}
        </div>

        <div class="form-group">
            {!! Form::label('gender', 'Gander:') !!}
            {!! Form::select('gender', ['Undefined' => 'Undefined', 'Male' => 'Male','Female' => 'Female'], null, ['class' => 'form-control']) !!}
        </div>

        <div class="form-group">
            {!! Form::label('email', 'Email:') !!}
            {!! Form::text('email', null, ['class' => 'form-control']) !!}
        </div>

        <div class="form-group">
            {!! Form::label('phone', 'Phone number:') !!}
            {!! Form::text('phone', null, ['class' => 'form-control bfh-phone']) !!}
        </div>

        <div class="form-group">
            {!! Form::label('birthday', 'Birthday:') !!}
            {!! Form::input('date', 'birthday', $user->birthday->format('d.m.Y'), ['class' => 'form-control']) !!}
        </div>

        <div class="form-group">
            {!! Form::label('description', 'Description:') !!}
            {!! Form::textarea('description', null, ['class' => 'form-control']) !!}
        </div>

        <div class="form-group">
            {!! Form::label('Avatar', 'Avatar') !!}
            {!! Form::file('file', null, ['class' => 'form-control']) !!}
        </div>

        {{-- Edit user role, only admin --}}
        @if (Auth::User()->hasRole('admin'))
            <div class="form-group">
                {!! Form::label('roles', 'Role:') !!}
                {!! Form::select('roles', $roles, $userRole, ['class' => 'form-control']) !!}
            </div>
        @endif

        <div class="form-group">
            {!! Form::submit('Edit user', ['class' => 'btn btn-primary form-control']) !!}
        </div>
    {!! Form::close() !!}

    @include('errors.list')
    </div>
@endsection