<div class="">
    @section ('cchart11_panel_title','Skills')
    @section ('cchart11_panel_body')
        <a href="#">
            <div>
                <p>
                    <strong>Math</strong>
                    <span class="pull-right text-muted">40% Complete</span>
                </p>

                <div>
                    @include('widgets.progress', array('animated'=> true, 'class'=>'warning', 'value'=>'40'))
                    <span class="sr-only">40% Complete (success)</span>
                </div>

            </div>
        </a>
        <a href="#">
            <div>
                <p>
                    <strong>Chemistry</strong>
                    <span class="pull-right text-muted">20% Complete</span>
                </p>

                <div>
                    @include('widgets.progress', array('animated'=> true, 'class'=>'danger', 'value'=>'20'))
                    <span class="sr-only">20% Complete</span>
                </div>

            </div>
        </a>
        <a href="#">
            <div>
                <p>
                    <strong>Languages</strong>
                    <span class="pull-right text-muted">60% Complete</span>
                </p>

                <div>
                    @include('widgets.progress', array('animated'=> true, 'class'=>'info', 'value'=>'60'))
                    <span class="sr-only">60% Complete (warning)</span>
                </div>

            </div>
        </a>
        <a href="#">
            <div>
                <p>
                    <strong>Biology</strong>
                    <span class="pull-right text-muted">80% Complete</span>
                </p>

                <div>
                    @include('widgets.progress', array('animated'=> true,'class'=>'success', 'value'=>'80'))
                    <span class="sr-only">80% Complete (danger)</span>
                </div>

            </div>
        </a>
        <hr>
        <a class="text-center" href="#">
            <strong>See All Skills</strong>
            <i class="fa fa-angle-right"></i>
        </a>
    @endsection
    @include('widgets.panel', array('header'=>true, 'as'=>'cchart11'))
</div>