@extends('layouts.dashboard')
@section('page_heading')

    <img src="/uploads/avatars/{{ $user->avatar }}" style="width: 100px; height: 100px; border-radius: 50%; margin-right: 25px"/>
    User {{  $user->name }}

    @if (Auth::User()->hasRole('admin') || Auth::User()->id == $user->id)
        <a href="{{ action('UsersController@edit', [$user->id]) }}" data-placement="top"
           data-toggle="tooltip" title="Edit" style="padding-left: 25px">
            <button class="btn btn-primary btn-xs" data-title="Edit" data-toggle="modal">
                <span class="glyphicon glyphicon-pencil"></span>
            </button>
        </a>
    @endif
@endsection

@section('section')

    <div class="col-lg-3 pull-right">
        @if (Auth::User()->id == $user->id && Auth::User()->hasRole('student'))
            @include('pages.users._skillsPanel')
        @endif
    </div>

    <div class="col-lg-9">
        @section ('inside_panel_title', 'Contact information')
        @section ('inside_panel_body')
            <address>
                <i class="fa fa-user"></i>  <strong>{{ $user->name }}</strong>,
                <i class="fa fa-calendar"></i>  {{$user->birthday->format('d.m.Y')}}<br><br>
                <i class="fa fa-map-marker"></i>  Latolankatu 9, Joensuu <br>
                <i class="fa fa-phone"></i>  {{$user->phone}} <br>
                <i class="fa fa-envelope-o"></i>  <a href="mailto:#">{{ $user->email }}</a>
            </address>
            <address>
                <button type="button" class="btn btn-primary btn-circle">
                    <i class="fa fa-facebook"></i>
                </button>
                <button type="button" class="btn btn-info btn-circle">
                    <i class="fa fa-twitter"></i>
                </button>
                <button type="button" class="btn btn-danger btn-circle">
                    <i class="fa fa-google-plus"></i>
                </button>
                <button type="button" class="btn btn-info btn-circle">
                    <i class="fa fa-skype"></i>
                </button>
            </address>
            <hr>
            <p>
                {{$user->description}}
            </p>
        @endsection
        @include('widgets.panel', array('header'=>true, 'as'=>'inside'))
    </div>

    @if (Auth::User()->id == $user->id)
        <div class="col-lg-9">
            @if (Auth::User()->hasRole('student') && !Auth::User()->studyTasks()->isEmpty())
                @section ('myTasks_panel_title', 'My tasks:')
            @section ('myTasks_panel_body')
                @include('pages.tasks._tasksTable',
                    array('class'=>'table-hover', 'taskList' => Auth::User()->studyTasks()))
            @endsection
            @include('widgets.panel', array('header'=>true, 'as'=>'myTasks'))

             {{--Teacher's created tasks--}}
            @elseif (Auth::User()->hasRole('teacher') && Auth::User()->tasks)
                @section ('myTasks_panel_title', 'My created tasks:')
            @section ('myTasks_panel_body')
                <div class="col-sm-12">
                    @include('pages.tasks._tasksTable',
                        array('class'=>'table-hover', 'taskList' => Auth::User()->tasks))
                </div>
            @endsection
            @include('widgets.panel', array('header'=>true, 'as'=>'myTasks'))
            @endif
        </div>

        <div class="col-lg-9">
            @if (Auth::User()->hasRole(['student', 'teacher']))
                @section ('myGroups_panel_title', 'My study groups:')
                @section ('myGroups_panel_body')
                    @if (Auth::User()->hasRole('teacher'))
                        @include('pages.groups._groupsTable',
                            array('class'=>'table-hover', 'groupList' => Auth::User()->groups))

                        <div class="col-sm-1 pull-right">
                            <a href="{{ action('GroupsController@create') }}" data-placement="top"
                               data-toggle="tooltip" title="Create">
                                <button type="button" class="btn btn-success btn-circle btn-lg"><i class="fa fa-plus"></i></button>
                            </a>
                        </div>
                    @else
                        @include('pages.groups._groupsTable',
                            array('class'=>'table-hover', 'groupList' => Auth::User()->studyInGroups))
                    @endif
                @endsection
                @include('widgets.panel', array('header'=>true, 'as'=>'myGroups'))
            @endif
        </div>
    @endif

@endsection