<li class="dropdown">
    <a class="dropdown-toggle" data-toggle="dropdown" href="#">
        <i class="fa fa-user fa-fw"></i>  <i class="fa fa-caret-down"></i>
    </a>
    <ul class="dropdown-menu dropdown-user">
        @if (Auth::guest())
            <li><a href="{{ url('/login') }}"><i class="fa fa-sign-in fa-fw"></i> Login</a>
            </li>
            <li><a href="{{ url('/register') }}"><i class="fa fa-user fa-fw"></i> Register</a>
            </li>
        @else
            <li><a href="{{ url('/users/'. Auth::user()->id) }}"><i class="fa fa-user fa-fw"></i> User Profile</a>
            </li>
            <li><a href="{{ url('/users/'. Auth::user()->id) . '/edit' }}"><i class="fa fa-gear fa-fw"></i> Settings</a>
            </li>
            <li class="divider"></li>
            <li><a href="{{ url ('logout') }}"><i class="fa fa-sign-out fa-fw"></i> Logout</a>
            </li>
        @endif
    </ul>
    <!-- /.dropdown-user -->
</li>