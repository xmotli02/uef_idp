@extends('layouts.dashboard')
@section('page_heading','Dashboard')
@section('section')

    <!-- /.row -->
    <div class="col-sm-12">

        <div class="row">
            <div class="col-sm-2">
                <img src="/uploads/avatars/{{ $user->avatar }}" style="width: 100px; height: 100px; border-radius: 50%; margin-right: 25px"/>
            </div>
            <div class="col-sm-10">
                <h1>Welcome, {{  $user->name }}!</h1>
                <p>Let's have a look at your studies.</p>
            </div>
        </div>
        <hr>


        <div class="row">
            <div class="col-lg-3 col-md-6">
                <div class="panel panel-primary">
                    <div class="panel-heading">
                        <div class="row">
                            <div class="col-xs-2">
                                <i class="fa fa-comments fa-4x"></i>
                            </div>
                            <div class="col-xs-10 text-right">
                                <div class="huge">5</div>
                                <div>New Activities!</div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-lg-3 col-md-6">
                <div class="panel panel-green">
                    <div class="panel-heading">
                        <div class="row">
                            <div class="col-xs-2">
                                <i class="fa fa-tasks fa-4x"></i>
                            </div>
                            <div class="col-xs-10 text-right">
                                <div class="huge">2</div>
                                <div>Groups completed!</div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-lg-3 col-md-6">
                <div class="panel panel-yellow">
                    <div class="panel-heading">
                        <div class="row">
                            <div class="col-xs-2">
                                <i class="fa fa-tasks fa-4x"></i>
                            </div>
                            <div class="col-xs-10 text-right">
                                <div class="huge">3</div>
                                <div>Groups in progress</div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-lg-3 col-md-6">
                <div class="panel panel-red">
                    <div class="panel-heading">
                        <div class="row">
                            <div class="col-xs-3">
                                <i class="fa fa-support fa-5x"></i>
                            </div>
                            <div class="col-xs-9 text-right">
                                <div class="huge">13</div>
                                <div>Not finished tasks</div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <hr>

        <!-- /.row -->
        <div class="row">
            <div class="col-lg-8">

            @section ('pane2_panel_title', 'Timeline')
            @section ('pane2_panel_body')
                <ul class="timeline">
                    <li>
                        <div class="timeline-badge danger" ><i class="fa fa-exclamation"></i>
                        </div>
                        <div class="timeline-panel">
                            <div class="timeline-heading">
                                <h4 class="timeline-title">Task two</h4>
                                <p><small class="text-muted"><i class="fa fa-clock-o"></i> in 11 hours</small>
                                </p>
                            </div>
                            <div class="timeline-body">
                                <p>In study group two</p>
                            </div>
                        </div>
                    </li>

                    <li class="timeline-inverted">
                        <div class="timeline-badge danger" ><i class="fa fa-exclamation"></i>
                        </div>
                        <div class="timeline-panel">
                            <div class="timeline-heading">
                                <h4 class="timeline-title">Task one</h4>
                                <p><small class="text-muted"><i class="fa fa-clock-o"></i> in 2 days</small>
                                </p>
                            </div>
                            <div class="timeline-body">
                                <p>In study group one</p>
                            </div>
                        </div>
                    </li>


                    <li>
                        <div class="timeline-badge warning"><i class="fa fa-credit-card"></i>
                        </div>
                        <div class="timeline-panel">
                            <div class="timeline-heading">
                                <h4 class="timeline-title">New groups are opening</h4>
                                <p><small class="text-muted"><i class="fa fa-clock-o"></i> in 2 days</small>
                                </p>
                            </div>
                            <div class="timeline-body">
                                <p>
                                    New group about Lorem is opening.
                                </p>
                                <p>
                                    New group about Iplsum is opening.
                                </p>
                            </div>
                        </div>
                    </li>

                    <li class="timeline-inverted">
                        <div class="timeline-badge success"><i class="fa fa-graduation-cap"></i>
                        </div>
                        <div class="timeline-panel">
                            <div class="timeline-heading">
                                <h4 class="timeline-title">School is having visitor from Local companies</h4>
                                <p><small class="text-muted"><i class="fa fa-clock-o"></i> in 1 week</small>
                                </p>
                            </div>
                            <div class="timeline-body">
                                <p>Meet successful people from local companies.</p>
                            </div>
                        </div>
                    </li>
                </ul>
            @endsection
            @include('widgets.panel', array('header'=>true, 'as'=>'pane2'))
        </div>

        <div class="col-lg-4">
            @if (Auth::User()->hasRole('student'))
                <div class="row">
                    <div class="col-sm-12">
                        @section ('cchart3_panel_title','Results')
                        @section ('cchart3_panel_body')
                            <div style="max-width:400px; margin:0 auto;">
                                <canvas class ="round" id="cdonut" width="250" height="195"></canvas>
                            </div>
                        @endsection
                        @include('widgets.panel', array('header'=>true, 'as'=>'cchart3'))
                    </div>

                    <div style="display: none">
                        @section ('cchart2_panel_title','Task types')
                        @section ('cchart2_panel_body')
                            <div style="max-width:400px; margin:0 auto;">@include('widgets.charts.cpiechart')</div>
                        @endsection
                        @include('widgets.panel', array('header'=>true, 'as'=>'cchart2'))
                    </div>

                    <div style="display: none">
                        @section ('cchart11_panel_title','Line Chart')
                        @section ('cchart11_panel_body')
                            @include('widgets.charts.clinechart')
                        @endsection
                        @include('widgets.panel', array('header'=>true, 'as'=>'cchart11'))
                    </div>
                </div>
            @endif

            @section ('pane1_panel_title', 'Notifications Panel')
            @section ('pane1_panel_body')
                <div class="list-group">
                    <a href="#" class="list-group-item">
                        <i class="fa fa-comment fa-fw"></i> First task was graded
                        <span class="pull-right text-muted small"><em>4 minutes ago</em>
                        </span>
                    </a>
                    <a href="#" class="list-group-item">
                        <i class="fa fa-twitter fa-fw"></i> 3 New Followers
                        <span class="pull-right text-muted small"><em>12 minutes ago</em>
                        </span>
                    </a>
                    <a href="#" class="list-group-item">
                        <i class="fa fa-envelope fa-fw"></i> Message Sent
                        <span class="pull-right text-muted small"><em>27 minutes ago</em>
                        </span>
                    </a>
                    <a href="#" class="list-group-item">
                        <i class="fa fa-tasks fa-fw"></i> New Task
                        <span class="pull-right text-muted small"><em>43 minutes ago</em>
                        </span>
                    </a>
                    <a href="#" class="list-group-item">
                        <i class="fa fa-upload fa-fw"></i> Server Rebooted
                        <span class="pull-right text-muted small"><em>11:32 AM</em>
                        </span>
                    </a>
                    <a href="#" class="list-group-item">
                        <i class="fa fa-bolt fa-fw"></i> Server Crashed!
                        <span class="pull-right text-muted small"><em>11:13 AM</em>
                        </span>
                    </a>
                    <a href="#" class="list-group-item">
                        <i class="fa fa-warning fa-fw"></i> Server Not Responding
                        <span class="pull-right text-muted small"><em>10:57 AM</em>
                        </span>
                    </a>
                </div>
                <!-- /.list-group -->
                <a href="#" class="btn btn-default btn-block">View All Alerts</a>
            @endsection
            @include('widgets.panel', array('header'=>true, 'as'=>'pane1'))
        </div>
    </div>

</div>
@stop
