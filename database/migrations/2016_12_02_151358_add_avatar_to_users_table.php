<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddAvatarToUsersTable extends Migration
{
    /**
     * Run the migrations.
     * Add paht to avatar
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function ($table) {
            $table->string('avatar')->default('default.jpg');
            $table->string('gender')->default('Undefined');
            $table->string('phone')->default('XXX XXX XXX');
            $table->timestamp('birthday');
            $table->text('description');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function ($table) {
            $table->dropColumn('avatar');
            $table->dropColumn('gender');
            $table->dropColumn('phone');
            $table->dropColumn('birthday');
            $table->dropColumn('description');
        });
    }
}
