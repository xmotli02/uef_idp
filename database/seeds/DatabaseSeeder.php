<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call(RolesTableSeeder::class);
        $this->call(UsersTableSeeder::class);

        $this->call(GroupTableSeeder::class);
        $this->call(TasksTableSeeder::class);

        $this->call(StudyMaterialsTableSeeder::class);
        $this->call(SolutionsTableSeeder::class);
        $this->call(FeedbackSolutionsTableSeeder::class);
    }
}
