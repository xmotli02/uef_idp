<?php

use Illuminate\Database\Seeder;

class SolutionsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('solutions')->delete();

        $student = App\User::where('name', 'Anna Bellomo')->first();
        $student2 = App\User::where('name', 'Lisa Trogus')->first();
        $student3 = App\User::where('name', 'Giada Lanzolla')->first();
        $student4 = App\User::where('name', 'Mathilde Chanteau')->first();
        $student5 = App\User::where('name', 'Guido Brouns')->first();

        $task = App\Task::find(1);
        $task2 = App\Task::find(2);
        $task3 = App\Task::find(3);
        $task4 = App\Task::find(4);
        $task5 = App\Task::find(5);
        $task6 = App\Task::find(6);


        //task
        App\Solution::create([
            'subject' => 'Lorem',
            'description' => 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit.
            In laoreet, magna id viverra tincidunt, sem odio bibendum justo, vel imperdiet
            sapien wisi sed libero. Aenean id metus id velit ullamcorper pulvinar. Duis ante
            orci, molestie vitae vehicula venenatis, tincidunt ac pede.',
            'filename' => 'phpQ9vSyD.pdf',
            'user_id' => $student->id,
            'task_id' => $task->id,
            'published_at' => '2016-11-01'
        ]);

        App\Solution::create([
            'subject' => 'Ipsum',
            'description' => 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit.
            In laoreet, magna id viverra tincidunt, sem odio bibendum justo, vel imperdiet
            sapien wisi sed libero. Aenean id metus id velit ullamcorper pulvinar. Duis ante
            orci, molestie vitae vehicula venenatis, tincidunt ac pede.',
            'filename' => 'phpQ9vSyD.pdf',
            'user_id' => $student2->id,
            'task_id' => $task->id,
            'published_at' => '2016-11-01'
        ]);

        App\Solution::create([
            'subject' => 'Lorem Ipsum 2',
            'description' => 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit.
            In laoreet, magna id viverra tincidunt, sem odio bibendum justo, vel imperdiet
            sapien wisi sed libero. Aenean id metus id velit ullamcorper pulvinar. Duis ante
            orci, molestie vitae vehicula venenatis, tincidunt ac pede.',
            'filename' => 'phpQ9vSyD.pdf',
            'user_id' => $student3->id,
            'task_id' => $task->id,
            'published_at' => '2016-11-02'
        ]);


        //task2
        App\Solution::create([
            'subject' => 'Lorem Ipsum 3',
            'description' => 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit.
            In laoreet, magna id viverra tincidunt, sem odio bibendum justo, vel imperdiet
            sapien wisi sed libero. Aenean id metus id velit ullamcorper pulvinar. Duis ante
            orci, molestie vitae vehicula venenatis, tincidunt ac pede.',
            'user_id' => $student->id,
            'task_id' => $task2->id,
            'published_at' => '2016-11-03'
        ]);

        App\Solution::create([
            'subject' => 'Lorem Ipsum 4',
            'description' => 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit.
            In laoreet, magna id viverra tincidunt, sem odio bibendum justo, vel imperdiet
            sapien wisi sed libero. Aenean id metus id velit ullamcorper pulvinar. Duis ante
            orci, molestie vitae vehicula venenatis, tincidunt ac pede.',
            'filename' => 'phpQ9vSyD.pdf',
            'user_id' => $student2->id,
            'task_id' => $task2->id,
            'published_at' => '2016-10-01'
        ]);


        //task3
        App\Solution::create([
            'subject' => 'Lorem Ipsum 5',
            'description' => 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit.
            In laoreet, magna id viverra tincidunt, sem odio bibendum justo, vel imperdiet
            sapien wisi sed libero. Aenean id metus id velit ullamcorper pulvinar. Duis ante
            orci, molestie vitae vehicula venenatis, tincidunt ac pede.',
            'filename' => 'phpQ9vSyD.pdf',
            'user_id' => $student3->id,
            'task_id' => $task3->id,
            'published_at' => '2016-11-07'
        ]);
        App\Solution::create([
            'subject' => 'Ipsum',
            'description' => 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit.
            In laoreet, magna id viverra tincidunt, sem odio bibendum justo, vel imperdiet
            sapien wisi sed libero. Aenean id metus id velit ullamcorper pulvinar. Duis ante
            orci, molestie vitae vehicula venenatis, tincidunt ac pede.',
            'user_id' => $student2->id,
            'task_id' => $task3->id,
            'published_at' => '2016-11-01'
        ]);

        //task4
        App\Solution::create([
            'subject' => 'Lorem Ipsum 5',
            'description' => 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit.
            In laoreet, magna id viverra tincidunt, sem odio bibendum justo, vel imperdiet
            sapien wisi sed libero. Aenean id metus id velit ullamcorper pulvinar. Duis ante
            orci, molestie vitae vehicula venenatis, tincidunt ac pede.',
            'filename' => 'phpQ9vSyD.pdf',
            'user_id' => $student->id,
            'task_id' => $task4->id,
            'published_at' => '2016-11-07'
        ]);

        //task5
        App\Solution::create([
            'subject' => 'Lorem Ipsum 5',
            'description' => 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit.
            In laoreet, magna id viverra tincidunt, sem odio bibendum justo, vel imperdiet
            sapien wisi sed libero. Aenean id metus id velit ullamcorper pulvinar. Duis ante
            orci, molestie vitae vehicula venenatis, tincidunt ac pede.',
            'filename' => 'phpQ9vSyD.pdf',
            'user_id' => $student4->id,
            'task_id' => $task5->id,
            'published_at' => '2016-11-07'
        ]);

        //task6
        App\Solution::create([
            'subject' => 'Lorem Ipsum 5',
            'description' => 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit.
            In laoreet, magna id viverra tincidunt, sem odio bibendum justo, vel imperdiet
            sapien wisi sed libero. Aenean id metus id velit ullamcorper pulvinar. Duis ante
            orci, molestie vitae vehicula venenatis, tincidunt ac pede.',
            'filename' => 'phpQ9vSyD.pdf',
            'user_id' => $student5->id,
            'task_id' => $task6->id,
            'published_at' => '2016-11-07'
        ]);
    }
}
