<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->delete();

        // Admin
        $admin = App\User::create([
            'name' => 'admin',
            'email' => 'admin@idp.com',
            'password' => bcrypt('admin'),
        ]);
        $adminRole = App\Role::where('name', 'admin')->first();
        $admin->roles()->save($adminRole);

        // Teachers
//        $teacher = App\User::create([
//            'name' => 'teacher',
//            'email' => 'teacher@idp.com',
//            'password' => bcrypt('teacher'),
//        ]);
//        $teacher->roles()->save($teacherRole);
//
//        $teacher2 = App\User::create([
//            'name' => 'Second teacher',
//            'email' => 'teacher2@idp.com',
//            'password' => bcrypt('teacher'),
//            'avatar' => '14814123942.png',
//        ]);
//        $teacher2->roles()->save($teacherRole);

        $teacher3 = App\User::create([
            'name' => 'Ing. Zdeněk Vašíček, Ph.D.',
            'email' => 'teacher@idp.com',
            'password' => bcrypt('teacher'),
            'avatar' => 'face-icon.png',
            'phone' => '541 141 165',
            'birthday' => '23.11.1988',
        ]);
        $teacherRole = App\Role::where('name', 'teacher')->first();
        $teacher3->roles()->save($teacherRole);

        $teacher4 = App\User::create([
            'name' => 'Prof. Ing. Tomáš Vojnar, Ph.D.',
            'email' => 'teacher4@idp.com',
            'password' => bcrypt('teacher'),
            'avatar' => 'face-icon.png',
            'phone' => '541 141 202',
            'birthday' => '23.11.1988',
        ]);
        $teacher4->roles()->save($teacherRole);

        $teacher5 = App\User::create([
            'name' => 'Ing. Jiří Jaroš, Ph.D.',
            'email' => 'teacher5@idp.com',
            'password' => bcrypt('teacher'),
            'avatar' => 'face-icon.png',
            'phone' => '541 141 207',
            'birthday' => '27.12.1988',
        ]);
        $teacher5->roles()->save($teacherRole);

        $teacher6 = App\User::create([
            'name' => 'Ing. Bohuslav Křena, Ph.D.',
            'email' => 'teacher6@idp.com',
            'password' => bcrypt('teacher'),
            'avatar' => 'face-icon.png',
            'phone' => '456 678 345',
            'birthday' => '23.11.1988',
        ]);
        $teacher6->roles()->save($teacherRole);

        $teacher7 = App\User::create([
            'name' => 'Ing. Aleš Smrčka, Ph.D.',
            'email' => 'teacher7@idp.com',
            'password' => bcrypt('teacher'),
            'avatar' => 'face-icon.png',
            'phone' => '541 141 186',
            'birthday' => '11.12.1991',
        ]);
        $teacher7->roles()->save($teacherRole);

        // Students
        $student3 = App\User::create([
            'name' => 'Anna Bellomo',
            'email' => 'student@idp.com',
            'password' => bcrypt('student'),
            'avatar' => '148153765911.jpg',
            'phone' => '541 141 186',
            'birthday' => '11.12.2003',
        ]);
        $studentRole = App\Role::where('name', 'student')->first();
        $student3->roles()->save($studentRole);

        $student4 = App\User::create([
            'name' => 'Lisa Trogus',
            'email' => 'student4@idp.com',
            'password' => bcrypt('student'),
            'avatar' => '148153767312.jpg',
            'phone' => '541 141 186',
            'birthday' => '11.12.2003',
        ]);
        $student4->roles()->save($studentRole);

        $student5 = App\User::create([
            'name' => 'Giada Lanzolla',
            'email' => 'student5@idp.com',
            'password' => bcrypt('student'),
            'avatar' => '148153773213.jpg',
            'phone' => '541 141 186',
            'birthday' => '11.12.2003',
        ]);
        $student5->roles()->save($studentRole);

        $student6 = App\User::create([
            'name' => 'Mathilde Chanteau',
            'email' => 'student6@idp.com',
            'password' => bcrypt('student'),
            'avatar' => '148153774714.jpg',
            'phone' => '541 141 186',
            'birthday' => '11.12.2003',
        ]);
        $student6->roles()->save($studentRole);

        $student7 = App\User::create([
            'name' => 'Guido Brouns',
            'email' => 'student7@idp.com',
            'password' => bcrypt('student'),
            'avatar' => '148153776115.jpg',
            'phone' => '541 141 186',
            'birthday' => '11.12.2003',
        ]);
        $student7->roles()->save($studentRole);

        $student8 = App\User::create([
            'name' => 'Alexandre Segura',
            'email' => 'student8@idp.com',
            'password' => bcrypt('student'),
            'phone' => '541 141 186',
            'birthday' => '11.12.2003',
        ]);
        $student8->roles()->save($studentRole);

        $student9 = App\User::create([
            'name' => 'Nico Gorter',
            'email' => 'student9@idp.com',
            'password' => bcrypt('student'),
            'phone' => '541 141 186',
            'birthday' => '11.12.2003',
        ]);
        $student9->roles()->save($studentRole);

        $student10 = App\User::create([
            'name' => 'Anna Lacombe',
            'email' => 'student10@idp.com',
            'password' => bcrypt('student'),
            'phone' => '541 141 186',
            'birthday' => '11.12.2003',
        ]);
        $student10->roles()->save($studentRole);

        $student11 = App\User::create([
            'name' => 'Clara Reinthaler',
            'email' => 'student11@idp.com',
            'password' => bcrypt('student'),
            'phone' => '541 141 186',
            'birthday' => '11.12.2003',
        ]);
        $student11->roles()->save($studentRole);

        $student12 = App\User::create([
            'name' => 'Heikki Katajala',
            'email' => 'student12@idp.com',
            'password' => bcrypt('student'),
            'phone' => '541 141 186',
            'birthday' => '11.12.2003',
        ]);
        $student12->roles()->save($studentRole);

        $student13 = App\User::create([
            'name' => 'Pilvi Hatakka',
            'email' => 'student13@idp.com',
            'password' => bcrypt('student'),
            'phone' => '541 141 186',
            'birthday' => '11.12.2003',
        ]);
        $student13->roles()->save($studentRole);

        $student14 = App\User::create([
            'name' => 'Bertalan Farkas',
            'email' => 'student14@idp.com',
            'password' => bcrypt('student'),
            'phone' => '541 141 186',
            'birthday' => '11.12.2003',
        ]);
        $student14->roles()->save($studentRole);

        $student15 = App\User::create([
            'name' => 'Karina Sedova',
            'email' => 'student15@idp.com',
            'password' => bcrypt('student'),
            'phone' => '541 141 186',
            'birthday' => '11.12.2003',
        ]);
        $student15->roles()->save($studentRole);

        $student16 = App\User::create([
            'name' => 'Arash Mirhashemi',
            'email' => 'student16@idp.com',
            'password' => bcrypt('student'),
            'phone' => '541 141 186',
            'birthday' => '11.12.2003',
        ]);
        $student16->roles()->save($studentRole);

        $student17 = App\User::create([
            'name' => 'Szabi Ka',
            'email' => 'student17@idp.com',
            'password' => bcrypt('student'),
            'phone' => '541 141 186',
            'birthday' => '11.12.2003',
        ]);
        $student17->roles()->save($studentRole);

        $student18 = App\User::create([
            'name' => 'Martina Rosato',
            'email' => 'student18@idp.com',
            'password' => bcrypt('student'),
            'phone' => '541 141 186',
            'birthday' => '11.12.2003',
        ]);
        $student18->roles()->save($studentRole);

        $student19 = App\User::create([
            'name' => 'Katharina Matern',
            'email' => 'student19@idp.com',
            'password' => bcrypt('student'),
            'phone' => '541 141 186',
            'birthday' => '11.12.2003',
        ]);
        $student19->roles()->save($studentRole);

        $student20 = App\User::create([
            'name' => 'Florent Arnaud',
            'email' => 'student20@idp.com',
            'password' => bcrypt('student'),
            'phone' => '541 141 186',
            'birthday' => '11.12.2003',
        ]);
        $student20->roles()->save($studentRole);

        $student21 = App\User::create([
            'name' => 'Olga Koltcova',
            'email' => 'student21@idp.com',
            'password' => bcrypt('student'),
            'phone' => '541 141 186',
            'birthday' => '11.12.2003',
        ]);
        $student21->roles()->save($studentRole);


    }
}
