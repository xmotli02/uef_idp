<?php

use Illuminate\Database\Seeder;

class FeedbackSolutionsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('solution_feedbacks')->delete();

        $teacher = App\User::where('name', 'Ing. Zdeněk Vašíček, Ph.D.')->first();
        $teacher2 = App\User::where('name', 'Prof. Ing. Tomáš Vojnar, Ph.D.')->first();

        $solution = App\Solution::find(1);
        $solution2 = App\Solution::find(2);
        $solution3 = App\Solution::find(3);
        $solution4 = App\Solution::find(4);

        App\SolutionFeedback::create([
            'comment' => 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit.
            In laoreet, magna id viverra tincidunt, sem odio bibendum justo, vel imperdiet
            sapien wisi sed libero. Aenean id metus id velit ullamcorper pulvinar. Duis ante
            orci, molestie vitae vehicula venenatis, tincidunt ac pede.',
            'user_id' => $teacher->id,
            'solution_id' => $solution->id,
            'points' => 5
        ]);

        App\SolutionFeedback::create([
            'comment' => 'Aenean id metus id velit ullamcorper pulvinar. Duis ante
            orci, molestie vitae vehicula venenatis, tincidunt ac pede.',
            'user_id' => $teacher->id,
            'solution_id' => $solution2->id,
            'points' => 5
        ]);

        App\SolutionFeedback::create([
            'comment' => 'Sem odio bibendum justo, vel imperdiet
            sapien wisi sed libero. Aenean id metus id velit ullamcorper pulvinar. Duis ante
            orci, molestie vitae vehicula venenatis, tincidunt ac pede.',
            'user_id' => $teacher->id,
            'solution_id' => $solution3->id,
            'points' => 5
        ]);

        App\SolutionFeedback::create([
            'comment' => 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit.
            In laoreet, magna id viverra tincidunt, sem odio bibendum justo, vel imperdiet
            sapien wisi sed libero. Aenean id metus id velit ullamcorper pulvinar. Duis ante
            orci, molestie vitae vehicula venenatis, tincidunt ac pede.',
            'user_id' => $teacher2->id,
            'solution_id' => $solution4->id,
            'points' => 5
        ]);
    }
}
