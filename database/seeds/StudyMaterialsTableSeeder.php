<?php

use Illuminate\Database\Seeder;

class StudyMaterialsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('materials')->delete();

        $teacher = App\User::where('name', 'Ing. Zdeněk Vašíček, Ph.D.')->first();
        $teacher2 = App\User::where('name', 'Prof. Ing. Tomáš Vojnar, Ph.D.')->first();
        $teacher3 = App\User::where('name', 'Ing. Jiří Jaroš, Ph.D.')->first();
        $teacher4 = App\User::where('name', 'Ing. Bohuslav Křena, Ph.D.')->first();
        $teacher5 = App\User::where('name', 'Ing. Aleš Smrčka, Ph.D.')->first();

        $group = App\Group::find(1);
        $group2 = App\Group::find(2);
        $group3 = App\Group::find(3);
        $group4 = App\Group::find(4);
        $group5 = App\Group::find(5);
        $group6 = App\Group::find(6);

        //group
        App\StudyMaterial::create([
            'name' => 'Lorem',
            'description' => 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit.
            In laoreet, magna id viverra tincidunt, sem odio bibendum justo, vel imperdiet
            sapien wisi sed libero. Aenean id metus id velit ullamcorper pulvinar. Duis ante
            orci, molestie vitae vehicula venenatis, tincidunt ac pede.',
            'filename' => 'phpQ9vSyD.pdf',
            'user_id' => $teacher->id,
            'group_id' => $group->id
        ]);

        App\StudyMaterial::create([
            'name' => 'Ipsum',
            'description' => 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit.
            In laoreet, magna id viverra tincidunt, sem odio bibendum justo, vel imperdiet
            sapien wisi sed libero. Aenean id metus id velit.',
            'filename' => 'phpQ9vSyD.pdf',
            'user_id' => $teacher2->id,
            'group_id' => $group->id
        ]);

        //group2
        App\StudyMaterial::create([
            'name' => 'dolor',
            'description' => 'Curabitur bibendum justo non orci. Donec quis nibh at felis congue commodo.
            Nulla non lectus sed nisl molestie malesuada',
            'filename' => 'phpQ9vSyD.pdf',
            'user_id' => $teacher->id,
            'group_id' => $group2->id
        ]);

        App\StudyMaterial::create([
            'name' => 'Donec',
            'description' => 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit.
            In laoreet, magna id viverra tincidunt..',
            'filename' => 'phpQ9vSyD.pdf',
            'user_id' => $teacher3->id,
            'group_id' => $group2->id
        ]);

        //group3
        App\StudyMaterial::create([
            'name' => 'Consectetuer',
            'description' => 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit.
            In laoreet, magna id viverra tincidunt..',
            'filename' => 'phpQ9vSyD.pdf',
            'user_id' => $teacher4->id,
            'group_id' => $group3->id
        ]);

        App\StudyMaterial::create([
            'name' => 'Viverra',
            'description' => 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit.
            In laoreet, magna id viverra tincidunt..',
            'filename' => 'phpQ9vSyD.pdf',
            'user_id' => $teacher5->id,
            'group_id' => $group3->id
        ]);

        //group4
        App\StudyMaterial::create([
            'name' => 'Amet',
            'description' => 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit.
            In laoreet, magna id viverra tincidunt..',
            'filename' => 'phpQ9vSyD.pdf',
            'user_id' => $teacher3->id,
            'group_id' => $group4->id
        ]);

        App\StudyMaterial::create([
            'name' => 'Magna',
            'description' => 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit.
            In laoreet, magna id viverra tincidunt..',
            'filename' => 'phpQ9vSyD.pdf',
            'user_id' => $teacher->id,
            'group_id' => $group4->id
        ]);

        //group5
        App\StudyMaterial::create([
            'name' => 'Sit amet',
            'description' => 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit.
            In laoreet, magna id viverra tincidunt..',
            'filename' => 'phpQ9vSyD.pdf',
            'user_id' => $teacher->id,
            'group_id' => $group5->id
        ]);

        App\StudyMaterial::create([
            'name' => 'Adipiscing',
            'description' => 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit.
            In laoreet, magna id viverra tincidunt..',
            'filename' => 'phpQ9vSyD.pdf',
            'user_id' => $teacher5->id,
            'group_id' => $group5->id
        ]);

        //group6
        App\StudyMaterial::create([
            'name' => 'Dolor sit',
            'description' => 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit.
            In laoreet, magna id viverra tincidunt..',
            'filename' => 'phpQ9vSyD.pdf',
            'user_id' => $teacher->id,
            'group_id' => $group6->id
        ]);

        App\StudyMaterial::create([
            'name' => 'Lorem ipsum',
            'description' => 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit.
            In laoreet, magna id viverra tincidunt..',
            'filename' => 'phpQ9vSyD.pdf',
            'user_id' => $teacher->id,
            'group_id' => $group6->id
        ]);
    }
}
