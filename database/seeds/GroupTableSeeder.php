<?php

use Illuminate\Database\Seeder;

class GroupTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('groups')->delete();

        $teacher = App\User::where('name', 'Ing. Zdeněk Vašíček, Ph.D.')->first();
        $teacher2 = App\User::where('name', 'Prof. Ing. Tomáš Vojnar, Ph.D.')->first();
        $teacher3 = App\User::where('name', 'Ing. Jiří Jaroš, Ph.D.')->first();
        $teacher4 = App\User::where('name', 'Ing. Bohuslav Křena, Ph.D.')->first();
        $teacher5 = App\User::where('name', 'Ing. Aleš Smrčka, Ph.D.')->first();

        $group1 = App\Group::create([
            'name' => 'English Literature',
            'description' => 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit.
            In laoreet, magna id viverra tincidunt, sem odio bibendum justo, vel imperdiet
            sapien wisi sed libero. Aenean id metus id velit ullamcorper pulvinar. Duis ante
            orci, molestie vitae vehicula venenatis, tincidunt ac pede.',
            'user_id' => $teacher->id
        ]);

        $group2 = App\Group::create([
            'name' => 'Google',
            'description' => 'Curabitur ligula sapien, pulvinar a vestibulum quis, facilisis vel
            sapien. Quisque tincidunt scelerisque libero. Duis bibendum, lectus ut viverra rhoncus,
            dolor nunc faucibus libero, eget facilisis enim ipsum id lacus. Etiam bibendum elit
            eget erat. Cras elementum. Fusce tellus.',
            'user_id' => $teacher->id
        ]);

        $group3 = App\Group::create([
            'name' => 'Games development',
            'description' => 'Class aptent taciti sociosqu ad litora torquent per conubia nostra, per
            inceptos hymenaeos. Donec vitae arcu. Nullam justo enim, consectetuer nec,
            ullamcorper ac, vestibulum in, elit.',
            'user_id' => $teacher3->id
        ]);

        $group4 = App\Group::create([
            'name' => 'Geography',
            'description' => 'Curabitur bibendum justo non orci. Donec quis nibh at felis congue commodo.
            Nulla non lectus sed nisl molestie malesuada.',
            'user_id' => $teacher2->id
        ]);

        $group5 = App\Group::create([
            'name' => 'Science',
            'description' => 'In enim a arcu imperdiet malesuada. Nam libero tempore, cum soluta nobis
            est eligendi optio cumque nihil impedit quo minus id quod maxime placeat facere
            possimus, omnis voluptas assumenda est, omnis dolor repellendus. Aliquam ornare
            wisi eu metus. Sed elit dui, pellentesque a, faucibus vel, interdum nec, diam.
            Nullam justo enim, consectetuer nec, ullamcorper ac, vestibulum in, elit. Sed
            vel lectus. Donec odio tempus molestie, porttitor ut, iaculis quis, sem. Aenean
            vel massa quis mauris vehicula lacinia. Phasellus faucibus molestie nisl. Nemo
            enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia
            consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Pellentesque
            habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas.
            Etiam quis quam.',
            'user_id' => $teacher2->id
        ]);

        $group6 = App\Group::create([
            'name' => 'Multimedia Technology',
            'description' => 'Curabitur ligula sapien, pulvinar a vestibulum quis, facilisis vel
            sapien. Quisque tincidunt scelerisque libero. Duis bibendum, lectus ut viverra rhoncus,
            dolor nunc faucibus libero, eget facilisis enim ipsum id lacus. Etiam bibendum elit
            eget erat. Cras elementum. Fusce tellus.',
            'user_id' => $teacher->id
        ]);

        $group7 = App\Group::create([
            'name' => '3D Printing',
            'description' => 'Class aptent taciti sociosqu ad litora torquent per conubia nostra, per
            inceptos hymenaeos. Donec vitae arcu. Nullam justo enim, consectetuer nec,
            ullamcorper ac, vestibulum in, elit.',
            'user_id' => $teacher3->id
        ]);

        $group8 = App\Group::create([
            'name' => 'Physics',
            'description' => 'Curabitur bibendum justo non orci. Donec quis nibh at felis congue commodo.
            Nulla non lectus sed nisl molestie malesuada.',
            'user_id' => $teacher2->id
        ]);

        $group9 = App\Group::create([
            'name' => 'Project Maths',
            'description' => 'Class aptent taciti sociosqu ad litora torquent per conubia nostra, per
            inceptos hymenaeos. Donec vitae arcu. Nullam justo enim, consectetuer nec,
            ullamcorper ac, vestibulum in, elit.',
            'user_id' => $teacher5->id
        ]);

        $group10 = App\Group::create([
            'name' => 'Tourism',
            'description' => 'Curabitur bibendum justo non orci. Donec quis nibh at felis congue commodo.
            Nulla non lectus sed nisl molestie malesuada.',
            'user_id' => $teacher4->id
        ]);

        $group11 = App\Group::create([
            'name' => 'Mental Health Studies',
            'description' => 'Curabitur bibendum justo non orci. Donec quis nibh at felis congue commodo.
            Nulla non lectus sed nisl molestie malesuada.',
            'user_id' => $teacher->id
        ]);


        $student = App\User::where('name', 'Anna Bellomo')->first();
        $student->studyInGroups()->save($group1);
        $student->studyInGroups()->save($group2);
        $student->studyInGroups()->save($group4);
        $student->studyInGroups()->save($group5);
        $student->studyInGroups()->save($group7);
        $student->studyInGroups()->save($group8);
        $student->studyInGroups()->save($group10);
        $student->studyInGroups()->save($group11);

        $student2 = App\User::where('name', 'Lisa Trogus')->first();
        $student2->studyInGroups()->save($group1);
        $student2->studyInGroups()->save($group3);
        $student2->studyInGroups()->save($group4);
        $student2->studyInGroups()->save($group11);

        $student3 = App\User::where('name', 'Giada Lanzolla')->first();
        $student3->studyInGroups()->save($group1);
        $student3->studyInGroups()->save($group3);
        $student3->studyInGroups()->save($group4);
        $student3->studyInGroups()->save($group7);
        $student3->studyInGroups()->save($group8);
        $student3->studyInGroups()->save($group9);

        $student4 = App\User::where('name', 'Mathilde Chanteau')->first();
        $student4->studyInGroups()->save($group1);
        $student4->studyInGroups()->save($group3);
        $student4->studyInGroups()->save($group4);
        $student4->studyInGroups()->save($group10);
        $student4->studyInGroups()->save($group6);

        $student5 = App\User::where('name', 'Guido Brouns')->first();
        $student5->studyInGroups()->save($group3);
        $student5->studyInGroups()->save($group10);
        $student5->studyInGroups()->save($group11);

        $student6 = App\User::where('name', 'Alexandre Segura')->first();
        $student6->studyInGroups()->save($group3);
        $student6->studyInGroups()->save($group1);
        $student6->studyInGroups()->save($group6);

        $student7 = App\User::where('name', 'Nico Gorter')->first();
        $student7->studyInGroups()->save($group3);
        $student7->studyInGroups()->save($group8);
        $student7->studyInGroups()->save($group11);

        $student8 = App\User::where('name', 'Clara Reinthaler')->first();
        $student8->studyInGroups()->save($group9);
        $student8->studyInGroups()->save($group10);
        $student8->studyInGroups()->save($group4);
    }
}
