<?php

use Illuminate\Database\Seeder;

class RolesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('roles')->delete();

        $adminRole = App\Role::create([
            'name' => 'admin',
            'display_name' => 'Admin',
            'description' => 'User is admin',
        ]);

        $teacherRole = App\Role::create([
            'name' => 'teacher',
            'display_name' => 'Teacher',
            'description' => 'User is teacher',
        ]);

        $studentRole = App\Role::create([
            'name' => 'student',
            'display_name' => 'Student',
            'description' => 'User is student',
        ]);
    }
}
