<?php

use Illuminate\Database\Seeder;

class TasksTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('tasks')->delete();

        $teacher = App\User::where('name', 'Ing. Zdeněk Vašíček, Ph.D.')->first();
        $teacher2 = App\User::where('name', 'Prof. Ing. Tomáš Vojnar, Ph.D.')->first();
        $teacher3 = App\User::where('name', 'Ing. Jiří Jaroš, Ph.D.')->first();
        $teacher4 = App\User::where('name', 'Ing. Bohuslav Křena, Ph.D.')->first();
        $teacher5 = App\User::where('name', 'Ing. Aleš Smrčka, Ph.D.')->first();

        $group = App\Group::where('name', 'English Literature')->first();
        $group2 = App\Group::where('name', 'Google')->first();
        $group3 = App\Group::where('name', 'Games development')->first();
        $group4 = App\Group::where('name', 'Geography')->first();
        $group5 = App\Group::where('name', 'Science')->first();
        $group6 = App\Group::where('name', 'Multimedia Technology')->first();
        $group7 = App\Group::where('name', '3D Printing')->first();
        $group8 = App\Group::where('name', 'Physics')->first();
        $group9 = App\Group::where('name', 'Project Maths')->first();
        $group10 = App\Group::where('name', 'Tourism')->first();
        $group11 = App\Group::where('name', 'Mental Health Studies')->first();

        // group1
        App\Task::create([
            'name' => 'Lorem',
            'description' => 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit.
            In laoreet, magna id viverra tincidunt, sem odio bibendum justo, vel imperdiet
            sapien wisi sed libero. Aenean id metus id velit ullamcorper pulvinar. Duis ante
            orci, molestie vitae vehicula venenatis, tincidunt ac pede.',
            'user_id' => $teacher->id,
            'group_id' => $group->id,
            'published_at' => '2016-11-01',
            'deadline' => '2016-12-16 22:00:00',
            'max_points' => 10
        ]);

        App\Task::create([
            'name' => 'Ipsum',
            'description' => 'Curabitur ligula sapien, pulvinar a vestibulum quis, facilisis vel
            sapien. Quisque tincidunt scelerisque libero. Duis bibendum, lectus ut viverra rhoncus,
            dolor nunc faucibus libero, eget facilisis enim ipsum id lacus. Etiam bibendum elit
            eget erat. Cras elementum. Fusce tellus.',
            'user_id' => $teacher->id,
            'group_id' => $group->id,
            'published_at' => '2016-11-05',
            'deadline' => '2016-12-19 23:59:59',
            'max_points' => 25
        ]);

        App\Task::create([
            'name' => 'dolor',
            'description' => 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit.
            In laoreet, magna id viverra tincidunt.',
            'user_id' => $teacher->id,
            'group_id' => $group->id,
            'published_at' => '2016-11-20',
            'deadline' => '2016-12-15 23:59:59',
            'max_points' => 8
        ]);

        App\Task::create([
            'name' => 'Next year',
            'description' => 'Class aptent taciti sociosqu ad litora torquent per conubia nostra, per
            inceptos hymenaeos. Donec vitae arcu. Nullam justo enim, consectetuer nec,
            ullamcorper ac, vestibulum in, elit.',
            'user_id' => $teacher2->id,
            'group_id' => $group->id,
            'published_at' => '2016-11-01',
            'deadline' => '2017-12-19 23:59:59',
            'max_points' => 15
        ]);

        App\Task::create([
            'name' => 'Curabitur',
            'description' => 'Curabitur bibendum justo non orci. Donec quis nibh at felis congue commodo.
            Nulla non lectus sed nisl molestie malesuada.',
            'user_id' => $teacher3->id,
            'group_id' => $group->id,
            'published_at' => '2016-10-20',
            'deadline' => '2016-12-31 23:59:59',
            'max_points' => 11
        ]);

        App\Task::create([
            'name' => 'Not published task',
            'description' => 'Curabitur bibendum justo non orci. Donec quis nibh at felis congue commodo.
            Nulla non lectus sed nisl molestie malesuada.',
            'user_id' => $teacher3->id,
            'group_id' => $group->id,
            'published_at' => '2016-12-20',
            'deadline' => '2016-12-31 23:59:59',
            'max_points' => 10
        ]);

        //group2
        App\Task::create([
            'name' => 'sit',
            'description' => 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit.
            In laoreet, magna id viverra tincidunt, sem odio bibendum justo, vel imperdiet
            sapien wisi sed libero. Aenean id metus id velit ullamcorper pulvinar. Duis ante
            orci, molestie vitae vehicula venenatis, tincidunt ac pede.',
            'user_id' => $teacher2->id,
            'group_id' => $group2->id,
            'published_at' => '2016-11-01',
            'deadline' => '2016-12-14 22:00:00',
            'max_points' => 10
        ]);

        App\Task::create([
            'name' => 'Second task',
            'description' => 'Curabitur ligula sapien, pulvinar a vestibulum quis, facilisis vel
            sapien. Quisque tincidunt scelerisque libero. Duis bibendum, lectus ut viverra rhoncus,
            dolor nunc faucibus libero, eget facilisis enim ipsum id lacus. Etiam bibendum elit
            eget erat. Cras elementum. Fusce tellus.',
            'user_id' => $teacher4->id,
            'group_id' => $group2->id,
            'published_at' => '2016-11-05',
            'deadline' => '2017-01-19 23:59:59',
            'max_points' => 25
        ]);

        App\Task::create([
            'name' => 'Amet',
            'description' => 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit.
            In laoreet, magna id viverra tincidunt.',
            'user_id' => $teacher5->id,
            'group_id' => $group2->id,
            'published_at' => '2016-11-20',
            'deadline' => '2017-01-15 23:59:59',
            'max_points' => 8
        ]);



        //group3
        App\Task::create([
            'name' => 'Sit',
            'description' => 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit.
            In laoreet, magna id viverra tincidunt, sem odio bibendum justo, vel imperdiet
            sapien wisi sed libero. Aenean id metus id velit ullamcorper pulvinar. Duis ante
            orci, molestie vitae vehicula venenatis, tincidunt ac pede.',
            'user_id' => $teacher->id,
            'group_id' => $group3->id,
            'published_at' => '2016-11-01',
            'deadline' => '2016-12-10 22:00:00',
            'max_points' => 10
        ]);

        App\Task::create([
            'name' => 'Ligula',
            'description' => 'Curabitur ligula sapien, pulvinar a vestibulum quis, facilisis vel
            sapien. Quisque tincidunt scelerisque libero. Duis bibendum, lectus ut viverra rhoncus,
            dolor nunc faucibus libero, eget facilisis enim ipsum id lacus. Etiam bibendum elit
            eget erat. Cras elementum. Fusce tellus.',
            'user_id' => $teacher2->id,
            'group_id' => $group3->id,
            'published_at' => '2016-11-05',
            'deadline' => '2016-12-19 23:59:59',
            'max_points' => 25
        ]);

        App\Task::create([
            'name' => 'Elit',
            'description' => 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit.
            In laoreet, magna id viverra tincidunt.',
            'user_id' => $teacher5->id,
            'group_id' => $group3->id,
            'published_at' => '2016-11-20',
            'deadline' => '2016-12-15 23:59:59',
            'max_points' => 8
        ]);


        //group4
        App\Task::create([
            'name' => 'Vel',
            'description' => 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit.
            In laoreet, magna id viverra tincidunt, sem odio bibendum justo, vel imperdiet
            sapien wisi sed libero. Aenean id metus id velit ullamcorper pulvinar. Duis ante
            orci, molestie vitae vehicula venenatis, tincidunt ac pede.',
            'user_id' => $teacher->id,
            'group_id' => $group4->id,
            'published_at' => '2016-11-01',
            'deadline' => '2016-12-27 22:00:00',
            'max_points' => 10
        ]);

        App\Task::create([
            'name' => 'Sapien',
            'description' => 'Curabitur ligula sapien, pulvinar a vestibulum quis, facilisis vel
            sapien. Quisque tincidunt scelerisque libero. Duis bibendum, lectus ut viverra rhoncus,
            dolor nunc faucibus libero, eget facilisis enim ipsum id lacus. Etiam bibendum elit
            eget erat. Cras elementum. Fusce tellus.',
            'user_id' => $teacher2->id,
            'group_id' => $group4->id,
            'published_at' => '2016-11-05',
            'deadline' => '2016-12-31 23:59:59',
            'max_points' => 25
        ]);

        App\Task::create([
            'name' => 'Magna',
            'description' => 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit.
            In laoreet, magna id viverra tincidunt.',
            'user_id' => $teacher4->id,
            'group_id' => $group4->id,
            'published_at' => '2016-11-20',
            'deadline' => '2016-12-15 23:59:59',
            'max_points' => 8
        ]);


        //group5
        App\Task::create([
            'name' => 'Lorem',
            'description' => 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit.
            In laoreet, magna id viverra tincidunt, sem odio bibendum justo, vel imperdiet
            sapien wisi sed libero. Aenean id metus id velit ullamcorper pulvinar. Duis ante
            orci, molestie vitae vehicula venenatis, tincidunt ac pede.',
            'user_id' => $teacher->id,
            'group_id' => $group5->id,
            'published_at' => '2016-11-01',
            'deadline' => '2016-12-17 22:00:00',
            'max_points' => 10
        ]);

        App\Task::create([
            'name' => 'Ipsum',
            'description' => 'Curabitur ligula sapien, pulvinar a vestibulum quis, facilisis vel
            sapien. Quisque tincidunt scelerisque libero. Duis bibendum, lectus ut viverra rhoncus,
            dolor nunc faucibus libero, eget facilisis enim ipsum id lacus. Etiam bibendum elit
            eget erat. Cras elementum. Fusce tellus.',
            'user_id' => $teacher2->id,
            'group_id' => $group5->id,
            'published_at' => '2016-11-05',
            'deadline' => '2016-12-19 23:59:59',
            'max_points' => 25
        ]);

        App\Task::create([
            'name' => 'Magna',
            'description' => 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit.
            In laoreet, magna id viverra tincidunt.',
            'user_id' => $teacher2->id,
            'group_id' => $group5->id,
            'published_at' => '2016-11-20',
            'deadline' => '2016-12-15 23:59:59',
            'max_points' => 8
        ]);


        //group6
        App\Task::create([
            'name' => 'Vivera',
            'description' => 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit.
            In laoreet, magna id viverra tincidunt, sem odio bibendum justo, vel imperdiet
            sapien wisi sed libero. Aenean id metus id velit ullamcorper pulvinar. Duis ante
            orci, molestie vitae vehicula venenatis, tincidunt ac pede.',
            'user_id' => $teacher->id,
            'group_id' => $group6->id,
            'published_at' => '2016-11-01',
            'deadline' => '2016-12-10 22:00:00',
            'max_points' => 10
        ]);

        App\Task::create([
            'name' => 'Fusce',
            'description' => 'Curabitur ligula sapien, pulvinar a vestibulum quis, facilisis vel
            sapien. Quisque tincidunt scelerisque libero. Duis bibendum, lectus ut viverra rhoncus,
            dolor nunc faucibus libero, eget facilisis enim ipsum id lacus. Etiam bibendum elit
            eget erat. Cras elementum. Fusce tellus.',
            'user_id' => $teacher2->id,
            'group_id' => $group6->id,
            'published_at' => '2016-11-05',
            'deadline' => '2016-12-15 23:59:59',
            'max_points' => 25
        ]);


        //group7
        App\Task::create([
            'name' => 'Dolor',
            'description' => 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit.
            In laoreet, magna id viverra tincidunt, sem odio bibendum justo, vel imperdiet
            sapien wisi sed libero. Aenean id metus id velit ullamcorper pulvinar. Duis ante
            orci, molestie vitae vehicula venenatis, tincidunt ac pede.',
            'user_id' => $teacher->id,
            'group_id' => $group7->id,
            'published_at' => '2016-11-01',
            'deadline' => '2016-12-13 22:00:00',
            'max_points' => 10
        ]);

        App\Task::create([
            'name' => 'libero',
            'description' => 'Curabitur ligula sapien, pulvinar a vestibulum quis, facilisis vel
            sapien. Quisque tincidunt scelerisque libero. Duis bibendum, lectus ut viverra rhoncus,
            dolor nunc faucibus libero, eget facilisis enim ipsum id lacus. Etiam bibendum elit
            eget erat. Cras elementum. Fusce tellus.',
            'user_id' => $teacher2->id,
            'group_id' => $group7->id,
            'published_at' => '2016-11-05',
            'deadline' => '2016-12-19 23:59:59',
            'max_points' => 25
        ]);

        //group8
        App\Task::create([
            'name' => 'Wisi',
            'description' => 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit.
            In laoreet, magna id viverra tincidunt, sem odio bibendum justo, vel imperdiet
            sapien wisi sed libero. Aenean id metus id velit ullamcorper pulvinar. Duis ante
            orci, molestie vitae vehicula venenatis, tincidunt ac pede.',
            'user_id' => $teacher3->id,
            'group_id' => $group8->id,
            'published_at' => '2016-11-01',
            'deadline' => '2016-12-10 22:00:00',
            'max_points' => 10
        ]);

        //group9
        App\Task::create([
            'name' => 'Sed',
            'description' => 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit.
            In laoreet, magna id viverra tincidunt, sem odio bibendum justo, vel imperdiet
            sapien wisi sed libero. Aenean id metus id velit ullamcorper pulvinar. Duis ante
            orci, molestie vitae vehicula venenatis, tincidunt ac pede.',
            'user_id' => $teacher->id,
            'group_id' => $group9->id,
            'published_at' => '2016-11-01',
            'deadline' => '2016-12-25 22:00:00',
            'max_points' => 10
        ]);

        //group10
        App\Task::create([
            'name' => 'Pede',
            'description' => 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit.
            In laoreet, magna id viverra tincidunt, sem odio bibendum justo, vel imperdiet
            sapien wisi sed libero. Aenean id metus id velit ullamcorper pulvinar. Duis ante
            orci, molestie vitae vehicula venenatis, tincidunt ac pede.',
            'user_id' => $teacher5->id,
            'group_id' => $group10->id,
            'published_at' => '2016-11-01',
            'deadline' => '2016-12-30 22:00:00',
            'max_points' => 10
        ]);

        //group11
        App\Task::create([
            'name' => 'Lorem ipsum',
            'description' => 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit.
            In laoreet, magna id viverra tincidunt, sem odio bibendum justo, vel imperdiet
            sapien wisi sed libero. Aenean id metus id velit ullamcorper pulvinar. Duis ante
            orci, molestie vitae vehicula venenatis, tincidunt ac pede.',
            'user_id' => $teacher5->id,
            'group_id' => $group11->id,
            'published_at' => '2016-11-01',
            'deadline' => '2016-12-20 22:00:00',
            'max_points' => 10
        ]);
    }
}
