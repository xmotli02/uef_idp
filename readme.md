## Synopsis

The goal of this project is to create student evaluation system to support 
new Finnish curriculum for elementary and high schools.

## Motivation

New curriculum is trying to inspire interest, curiosity and supporting 
them in field of studies by their choice, but on the other hand 
is also giving them much more responsibilities. Stereotype of classical 
school subjects should step by step disappear from schools. 
To reach this goal, we decided to create a web application that both 
pupils and teachers will use. Idea of our application it is that 
teachers will be maintaining study groups following variety of phenomenons.

## Installation

You need to create VM using vagrant. In command line run `vagrant up`. 
Connect to VM using `vagrant ssh` and change directory to 
projectsource/ (`cd projectsource`). 

After that you need to run migrations to create all DB tables. 
Run `php artisan migrate` and to seed all tables with some data 
run `php artisan db:seed`.

## License

[MIT license](http://opensource.org/licenses/MIT).