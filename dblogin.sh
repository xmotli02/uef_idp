#!/bin/bash

## Location of .env file
FILE="/home/vagrant/uef-idp/.env"

regexDB='(DB_DATABASE=)(\w*)'
regexUSR='(DB_USERNAME=)(\w*)'
regexPASS='(DB_PASSWORD=)(\w*)'


while read -r line
do
    if [[ $line =~ $regexDB ]]; then
		DB_DATABASE="${BASH_REMATCH[2]}"
	elif [[ $line =~ $regexUSR ]]; then
		DB_USERNAME="${BASH_REMATCH[2]}"
	elif [[ $line =~ $regexPASS ]]; then
		DB_PASSWORD="${BASH_REMATCH[2]}"
	fi
done < "$FILE"

echo "Connecting: $DB_USERNAME@$DB_DATABASE"

mysql -u $DB_USERNAME -p$DB_PASSWORD $DB_DATABASE
